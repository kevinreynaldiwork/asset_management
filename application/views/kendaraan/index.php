<script>
    $(document).ready(function(){
        $('#show_info').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
            },
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                //panggil method ajax list dengan ajax
                "url": "<?php echo base_url(); ?>kendaraan/fetch_kendaraan_admin",
                "type": "POST"
            }
        });
    });

</script>

<div class="row">
    <div class="col-md-12">
        <h3> Selamat datang di halaman kendaraan</h3>
    </div>
</div>
<hr>
<!-- Button Add asset -->
    <div class="col-md-12 mt-2 mb-2">
    <a href="<?= base_url() ?>kendaraan/add_kendaraan_form" class="btn btn-dark" role="button" >Tambah Kendaraan</a>
    </div>
    <!-- Datatable -->
    <div class="col-md-12 mt-2 mb-2">
        <div class="table-responsive">
            <table id="show_info" class="table table-striped data-table datatable-ajax" style="width: 100%">
            <thead>
                <tr>
                    <th>Kode Asset</th>
                    <th>Nama Asset</th>
                    <th>Nopol</th>
                    <th>Kategori</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>

    </div>
    <!-- Modal for delete kendaraan -->
    <div class="modal fade" id="delete_asset" tabindex="-1" aria-labelledby="delete_modal" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Asset</h5>
                         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="post" action="../kendaraan/delete_kendaraan">
                        <div class="row">
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="kode" style="width:100%;" id="code_delete" readonly> 
                                <label style="font-weight:bold; color:black;">Code</label> </div>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="alasan_delete"
                                        placeholder="Input Asset Name Here" style="width:100%;" required> <label
                                        style="font-weight:bold; color:black;">Reason
                                        </label> </div>
                            </div>
                        </div>
                        <br>
                        <div style="text-align:center;">
                            <button type="submit" class="btn btn-dark"> Delete </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

 <!-- Modal Untuk perbaikan Asset -->
 <div class="modal fade" id="perbaikan_asset" tabindex="-1" aria-labelledby="perbaikan_asset" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title-perbaikan-asset">Perbaikan Asset</h5>
                         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" onclick="window.location.reload();"></button>
                </div>
                <div class="modal-body">
                    <form method="post" action="../kendaraan/add_perbaikan" enctype="multipart/form-data">
                    <div class="container">
                        <h3 style="text-align:center;">Form Perbaikan Asset Kendaraan </h3>
                        <div class="row">
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="kode"
                                        placeholder="Input Code Here" style="width:100%;" id="perbaikanAssetCode" readonly> <label
                                        style="font-weight:bold; color:black;">
                                        Code</label> </div>
                            </div>  
                        <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="date" name="tgl_kejadian"
                                        placeholder="Input Procurement Date Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Incident
                                        Date </label> </div>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="kronologi"
                                        placeholder="Input Asset Code Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">
                                        Chronology</label> </div>
                            </div>

                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="kondisi"
                                        placeholder="Input Asset Name Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Asset
                                        Condition</label> </div>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="action_plan"
                                        placeholder="Input Asset Name Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Action
                                        Plan</label> </div>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="RAB"
                                        placeholder="Input Asset Name Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">
                                        RAB</label> </div>
                            </div>

                            <div class="col-md-12 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Upload Photos</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <input type="file" id="file-input" accept="image/png, image/jpeg" onchange="preview()" name="userfile[]" multiple>
                                <label for="file-input"><i class="bi bi-upload"></i>Choose Photo</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <p for="num-of-files" id="num-of-files">No Files Chosen</p>
                                <div id="images"></div>
                            </div>

                        </div>
                        <br>
                        <div style="text-align:center;">
                            <button type="submit" class="btn btn-dark"> Submit </button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Untuk edit data kendaraan -->
    <div class="modal fade" id="edit_kendaraan" tabindex="-1" aria-labelledby="edit_kendaraan" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Perbaikan Asset</h5>
                         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="post" action="../kendaraan/edit_kendaraan" enctype="multipart/form-data">
                    <div class="row">
                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="kode"
                                         style="width:100%;" id="asset_code_edit" readonly> <label
                                        style="font-weight:bold; color:black;">Asset
                                        Code</label> </div>
                            </div>

                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="nama"
                                        placeholder="Input Asset Name Here" style="width:100%;" id="nama_asset_edit"> <label
                                        style="font-weight:bold; color:black;" >Asset
                                        Name</label> </div>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="lokasi"
                                        placeholder="Input Location Here" style="width:100%;" id="lokasi_edit"> <label
                                        style="font-weight:bold; color:black;">Location
                                    </label> </div>
                            </div>

                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="date" name="tgl_pengadaan"
                                        placeholder="Input Procurement Date Here" style="width:100%;" id="tgl_pengadaan_edit"> <label
                                        style="font-weight:bold; color:black;">Procurement
                                        Date </label> </div>
                            </div>
                            <div class="col-md-6 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Asset Type</label>
                                <select class="form-select" id="jenis_edit" name="jenis">
                                    <option value="Tetap" selected>Tetap</option>
                                    <option value="Bergerak">Bergerak</option>
                                </select>
                            </div>

                            <div class="col-md-6 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Precondition</label>
                                <select class="form-select" id="kondisi_edit" name="kondisi">
                                    <option value="Baik" selected>Baik</option>
                                    <option value="Rusak">Rusak</option>
                                </select>
                            </div>
                            

                            <div class="col-md-4 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="nopol"
                                    placeholder="Input Police Number Here" style="width:100%;" id="nopol_edit"> <label
                                    style="font-weight:bold; color:black;">Police Number </label> </div>
                            </div>

                            
                            <div class="col-md-4 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="nomor_mesin"
                                    placeholder="Input Machine Numver Here" style="width:100%;" id="nomor_mesin_edit"> <label
                                    style="font-weight:bold; color:black;">Machine Number</label> </div>
                            </div>

                            <div class="col-md-4 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Category</label>
                                <select class="form-select" id="kategori_edit" name="kategori">
                                    <option value="mobil" selected>Mobil</option>
                                    <option value="motor">Motor</option>
                                </select>
                            </div>

                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="date" name="durasi_pajak"
                                        placeholder="Input Tax Validity Period Here" style="width:100%;" id="durasi_pajak_edit"> <label
                                        style="font-weight:bold; color:black;">Tax validity period </label> </div>
                            </div>
                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="date" name="durasi_plat"
                                        placeholder="Input Plate Validity Period Here" style="width:100%;" id="durasi_plat_edit"> <label
                                        style="font-weight:bold; color:black;">Plate validity period </label> </div>
                            </div>

                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="pic"
                                        placeholder="Input PIC BPKB Here" style="width:100%;" id="PIC_edit"> <label
                                        style="font-weight:bold; color:black;">PIC
                                        BPKB</label> </div>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Upload Photos</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <input type="file" id="file-input-edit" accept="image/png, image/jpeg" onchange="preview2()" name="userfile[]" multiple>
                                <label for="file-input-edit"><i class="bi bi-upload"></i>Choose Photo</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <p for="num-of-files-edit" id="num-of-files-edit">No Files Chosen</p>
                                <div id="images-edit"></div>
                            </div>

                        </div>
                        <br>
                        <div style="text-align:center;">
                            <button type="submit" class="btn btn-dark"> Submit </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>
<!-- Datatable -->


<script>
$('#edit_kendaraan').on('hidden.bs.modal', function () {
    location.reload();
})
$('#perbaikan_asset').on('hidden.bs.modal', function () {
    location.reload();
})
$('#delete_asset').on('hidden.bs.modal', function () {
    location.reload();
})
function deleteClick (obj) {
      var rowID = $(obj).attr('data-id');
      document.getElementById("code_delete").value = rowID;
}

function editKendaraan(obj){
    var rowID = $(obj).attr('data-id');
    document.getElementById("asset_code_edit").value = rowID;
    $.ajax({
            url: "../kendaraan/get_kendaraan_json",
            type: "POST",
            data: {
                kode: rowID
            },
            dataType: 'json',
            success: function(data) {
                console.log(data);
                document.getElementById("nama_asset_edit").value = data.nama;
                document.getElementById("lokasi_edit").value = data.lokasi;
                document.getElementById("tgl_pengadaan_edit").value = data.tanggal_terima;
                var coba = data.info_asset;
                var obj = JSON.parse(coba);
                console.log(obj);
                document.getElementById("jenis_edit").value = obj.jenis;
                document.getElementById("kondisi_edit").value = obj.kondisi;
                document.getElementById("nopol_edit").value = obj.nopol;
                document.getElementById("nomor_mesin_edit").value = obj.nomor_mesin;
                document.getElementById("kategori_edit").value = data.kategori;
                document.getElementById("durasi_pajak_edit").value = obj.durasi_pajak;
                document.getElementById("durasi_plat_edit").value = obj.durasi_plat;
                document.getElementById("PIC_edit").value = obj.pic;
                var getnamepict = data.foto_asset;
                var imagetoJson = JSON.parse(getnamepict);
                console.log(imagetoJson);
                if(imagetoJson[0] !=""){
                    let numOfFiles = document.getElementById("num-of-files-edit");
                    numOfFiles.textContent = `${imagetoJson.length} Files Selected`;
                    for(var i = 0; i< imagetoJson.length; i++){
                        var img = document.createElement('img')
                        img.src = "../assets/img/"+imagetoJson[i];
                        img.style = "width:149px;height:101px;";
                        document.getElementById('images-edit').appendChild(img);
                        var nickname = document.createElement('p');
                        nickname.style="font-size:10px;margin-left:35px;"
                        var node = document.createTextNode(imagetoJson[i]);
                        nickname.appendChild(node);
                        document.getElementById('images-edit').appendChild(nickname);
                    }   
                }
                // jgn lupa di decode
            }
        });
}

function perbaikanAsset(obj){
    var rowID = $(obj).attr('data-id');
    document.getElementById("perbaikanAssetCode").value = rowID;
}

function preview(){
        let fileInput = document.getElementById("file-input");
        let imageContainer = document.getElementById("images");
        let numOfFiles = document.getElementById("num-of-files");
        imageContainer.innerHTML = "";
        numOfFiles.textContent = `${fileInput.files.length} Files Selected`;
        for(i of fileInput.files){
            let reader = new FileReader();
            let figure = document.createElement("figure");
            let figCap = document.createElement("figcaption");
            figCap.innerText = i.name;
            figure.appendChild(figCap);
            reader.onload=()=>{
                let img = document.createElement("img");
                img.setAttribute("src",reader.result);
                figure.insertBefore(img,figCap);
             }
            imageContainer.appendChild(figure);
            reader.readAsDataURL(i);
            }


}

function preview2(){
        let fileInput = document.getElementById("file-input-edit");
        let imageContainer = document.getElementById("images-edit");
        let numOfFiles = document.getElementById("num-of-files-edit");
        imageContainer.innerHTML = "";
        numOfFiles.textContent = `${fileInput.files.length} Files Selected`;
        for(i of fileInput.files){
            let reader = new FileReader();
            let figure = document.createElement("figure");
            let figCap = document.createElement("figcaption");
            figCap.innerText = i.name;
            figure.appendChild(figCap);
            reader.onload=()=>{
                let img = document.createElement("img");
                img.setAttribute("src",reader.result);
                figure.insertBefore(img,figCap);
             }
            imageContainer.appendChild(figure);
            reader.readAsDataURL(i);
            }
}
</script>
