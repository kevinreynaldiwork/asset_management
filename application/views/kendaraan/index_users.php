<script>
    $(document).ready(function(){
        $('#posts').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
            },
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                //panggil method ajax list dengan ajax
                "url": "<?php echo base_url(); ?>kendaraan/fetch_kendaraan_user",
                "type": "POST"
            }
        });
    });

</script>

<div class="card bg-dark text-white">
  <img class="card-img" src="../assets/img/Bg/bg4.jpg" alt="Card image" style="height:30rem;">
  <div class="card-img-overlay">
    <div class="body-text2">
        <h3>Welcome to </h3>
        <br>
        <h1>Kendaraan</h1>
        <br>
        <h1>Page</h1>
        <br>
    </div>
  </div>
</div>

<!-- data table -->
<div class="col-md-12 mt-2 mb-2">
        <div class="table-responsive">
            <table id="posts" class="table table-striped data-table datatable-ajax" style="width: 100%">
            <thead>
                <tr>
                    <th>Kode Asset</th>
                    <th>Nama Asset</th>
                    <th>Nopol</th>
                    <th>Kategori</th>
                    <th>Date</th>
                    <th>User</th>
                    <th>Department</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>

    </div>

<!-- Modal -->
    <div class="modal fade" id="detail-modal" tabindex="-1" role="dialog" aria-labelledby="detail-modal" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="detail-label">Detail Aset</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-3">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                                aria-orientation="vertical">
                                <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home"
                                    role="tab" aria-controls="v-pills-home" aria-selected="true">Detail</a>
                                <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile"
                                    role="tab" aria-controls="v-pills-profile" aria-selected="false">History</a>
                            </div>
                        </div>
                        <div class="col-9">
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel"
                                    aria-labelledby="v-pills-home-tab">
                                    <div class="row">
                                        <div class="col-md-4 mb-2">
                                            <div id="carouselExampleIndicators" class="carousel slide"
                                                data-ride="carousel">
                                                <div class="carousel-inner" id="gambar-carousel">
                                                    
                                                </div>
                                                <a class="carousel-control-prev" href="#carouselExampleIndicators"
                                                    role="button" data-slide="prev">
                                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="carousel-control-next" href="#carouselExampleIndicators"
                                                    role="button" data-slide="next">
                                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-8 mb-2">
                                            <div class="card text-center">
                                                <div class="card-header"
                                                    style="background-color:rgb(67, 67, 196);color:white;">
                                                    <span id="nama_asset_detail" style="font-size: large;"></span>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="content-detail">
                                                                <p style="margin-bottom:0px">Kode Asset</p>
                                                                <p><b><span id="kode_asset_detail"></span></b></p>
                                                            </div>
                                                            <div class="content-detail">
                                                                <p style="margin-bottom:0px">Lokasi</p>
                                                                <p><b><span id="lokasi_detail"></span></b></p>
                                                            </div>
                                                            <div class="content-detail">
                                                                <p style="margin-bottom:0px">Tanggal Pengadaan</p>
                                                                <p><b> <span id="tgl_detail"></span></b></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 ">
                                                            <div class="content-detail">
                                                                <p style="margin-bottom:0px">Jenis Asset</p>
                                                                <p><b> <span id="jenis_detail"></span> </b></p>
                                                            </div>
                                                            <div class="content-detail">
                                                                <p style="margin-bottom:0px">Kondisi Awal</p>
                                                                <p><b><span id="kondisi_detail"></span></b></p>
                                                            </div>
                                                            <div class="content-detail">
                                                                <p style="margin-bottom:0px">Nomor Polisi</p>
                                                                <p><b><span id="nopol_detail"></span></b></p>
                                                            </div>
                                                            <div class="content-detail">
                                                                <p style="margin-bottom:0px">Nomor Mesin</p>
                                                                <p><b> <span id="mesin_detail"></span></b></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 ">
                                                            <div class="content-detail">
                                                                <p style="margin-bottom:0px">Kategori</p>
                                                                <p><b> <span id="kategori_detail"></span></b></p>
                                                            </div>
                                                            <div class="content-detail">
                                                                <p style="margin-bottom:0px">Masa Pajak</p>
                                                                <p><b><span id="masapajak_detail"></span></b></p>
                                                            </div>
                                                            <div class="content-detail">
                                                                <p style="margin-bottom:0px">Masa Berlaku</p>
                                                                <p><b><span id="masaberlaku_detail"></span></b></p>
                                                            </div>
                                                            <div class="content-detail">
                                                                <p style="margin-bottom:0px">PIC BPKB</p>
                                                                <p><b><span id="PIC_detail"></span></b></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="v-pills-profile" role="tabpanel"
                                    aria-labelledby="v-pills-profile-tab">
                                    <div class="table-responsive">
                                        <table id="posts2" class="table table-striped data-table datatable-ajax"
                                            style="width: 100%">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Tanggal</th>
                                                    <th>Kegiatan</th>
                                                    <th>Ketrangan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="add-delete-button">
                                        <!-- tempat memunculkan tombol delete -->
                                        <br>
                                    </div>

                                </div>
                                <!-- Batas -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<!-- Modal -->
<div class="modal fade" id="history-modal" tabindex="-1" role="dialog" aria-labelledby="history" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="detail-label">Detail Perbaikan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-7">
            <!-- Pembatas -->
            <div class="history-contet">
              <div class="row">
                <div class="col-md-5">
                  <p><b>Tanggal Kejadian : </b></p>
                </div>
                <div class="col-md-7">
                  <p><span id="tanggal_history"></span></p>
                </div>
              </div>
            </div>
            <!-- Pembatas -->
            <div class="history-contet">
              <div class="row">
                <div class="col-md-5">
                  <p><b>Kronologi :</b></p>
                </div>
                <div class="col-md-7">
                  <p><span id="kronologi_history"></span></p>
                </div>
              </div>
            </div>
            <!-- Pembatas -->
            <div class="history-contet">
              <div class="row">
                <div class="col-md-5">
                  <p><b>Kondisi Asset : </b></p>
                </div>
                <div class="col-md-7">
                  <p><span id="kondisi_history"></span></p>
                </div>
              </div>
            </div>
            <!-- Pembatas -->
            <div class="history-contet">
              <div class="row">
                <div class="col-md-5">
                  <p><b>Action Plan : </b></p>
                </div>
                <div class="col-md-7">
                  <p><span id="aksi_history"></span> </p>
                </div>
              </div>
            </div>
            <!-- Pembatas -->
            <div class="history-contet">
              <div class="row">
                <div class="col-md-5">
                  <p><b>RAB : </b></p>
                </div>
                <div class="col-md-7">
                  <p><span id="rab_history"></span> </p>
                </div>
              </div>
            </div>
            <!-- Pembatas -->
          </div>
          <div class="col-md-5">
            <p><b>Foto Kondisi Asset</b></p>
            <div class="card">
              <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel2">
                <div class="carousel-inner" id="gambar-carousel2">
                  <!-- Masukan ke sini -->
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="penghapusan-modal" tabindex="-1" role="dialog" aria-labelledby="penghapusan-modal"
  aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="detail-label">Detail Penghapusan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
            <div class="penghapusan-content">
              <p><b>Tanggal Penghapusan : </b></p>
              <p><span id="tgl-delete"></span> </p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="penghapusan-content">
              <p><b>Alasan Dihapus : </b></p>
              <p><span id="alasan-delete"></span> </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$('#history-modal').on('hidden.bs.modal', function () {
    location.reload();
});
$('#detail-modal').on('hidden.bs.modal', function () {
    location.reload();
});
function getDetail(obj){
    var rowID = $(obj).attr('data-id');
    console.log(rowID);
    document.getElementById("kode_asset_detail").innerHTML = rowID;
    $.ajax({
            url: "../kendaraan/get_kendaraan_json",
            type: "POST",
            data: {
                kode: rowID
            },
            dataType: 'json',
            success: function(data) {
                console.log(data);
                if(data.status == 'deleted'){
                    var link = document.createElement('a');
                    link.className="btn btn-danger";
                    link.setAttribute("name", "penghapusan-modal");
                    link.setAttribute("data-toggle", "modal");
                    link.setAttribute("data-target", "#penghapusan-modal");
                    link.setAttribute("data-id", rowID);
                    link.setAttribute("onclick", "get_delete_item(this)");
                    var node = document.createTextNode("Alasan Penghapusan");
                    link.appendChild(node);
                    document.getElementById('add-delete-button').appendChild(link);
                }
                document.getElementById("nama_asset_detail").innerHTML = data.nama;
                document.getElementById("lokasi_detail").innerHTML = data.lokasi;
                document.getElementById("tgl_detail").innerHTML = data.tanggal_terima;
                var coba = data.info_asset;
                var obj = JSON.parse(coba);
                console.log(obj);
                document.getElementById("jenis_detail").innerHTML = obj.jenis;
                document.getElementById("kondisi_detail").innerHTML = obj.kondisi;
                document.getElementById("nopol_detail").innerHTML = obj.nopol;
                document.getElementById("mesin_detail").innerHTML = obj.nomor_mesin;
                document.getElementById("kategori_detail").innerHTML = data.kategori;
                document.getElementById("masapajak_detail").innerHTML = obj.durasi_pajak;
                document.getElementById("masaberlaku_detail").innerHTML = obj.durasi_plat;
                document.getElementById("PIC_detail").innerHTML = obj.pic;
                var getnamepict = data.foto_asset;
                var imagetoJson = JSON.parse(getnamepict);
                console.log(imagetoJson);
                for(var i = 0; i< imagetoJson.length; i++){
                    if( i == 0 ){
                        var div = document.createElement('div');
                        div.className  = "carousel-item active";
                        var img = document.createElement('img');
                        img.className  = "d-block  w-100";
                        img.src = "../assets/img/"+imagetoJson[i];
                        img.alt = "Slide";
                        document.getElementById('gambar-carousel').appendChild(div).appendChild(img);
                    }
                    else{
                        var div = document.createElement('div');
                        div.className  = "carousel-item";
                        var img = document.createElement('img');
                        img.className  = "d-block w-100";
                        img.src = "../assets/img/" + imagetoJson[i];;
                        img.alt = "Slide";
                        document.getElementById('gambar-carousel').appendChild(div).appendChild(img);
                    }
                }
            }
    });
    $('#posts2').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
            },
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "data": {
                    kode:rowID
                },
                "url": "<?php echo base_url(); ?>asset/fetch_history_asset",
                "type": "POST"
            }
        });
}
function getDetailHistory(obj){
    var rowID2 = $(obj).attr('data-id');
    console.log(rowID2);
    $.ajax({
            url: "../asset/get_detail_history_json",
            type: "POST",
            data: {
                id_history: rowID2
            },
            dataType: 'json',
            success: function(data) {
                console.log(data);
                document.getElementById("tanggal_history").innerHTML = data.tanggal_kegiatan;
                document.getElementById("kronologi_history").innerHTML = data.kronologi;
                document.getElementById("kondisi_history").innerHTML = data.kondisi;
                document.getElementById("aksi_history").innerHTML = data.action_plan;
                document.getElementById("rab_history").innerHTML = data.RAB;
                var getnamepict2 = data.foto_kegiatan;
                var imagetoJson2 = JSON.parse(getnamepict2);
                console.log(imagetoJson2);
                if(imagetoJson2.length === 0){
                    var parap = document.createElement('h3');
                    parap.setAttribute("style", "text-align: center;")
                    var node2 = document.createTextNode("Tidak ada Gambar");
                    parap.appendChild(node2);
                    document.getElementById('gambar-carousel2').appendChild(parap);
                }else{
                    for(var i = 0; i< imagetoJson2.length; i++){
                        if( i == 0 ){
                            var div = document.createElement('div');
                            div.className  = "carousel-item active";
                            var img = document.createElement('img');
                            img.className  = "d-block w-100";
                            img.src = "../assets/img/"+imagetoJson2[i];
                            img.alt = "Slide";
                            document.getElementById('gambar-carousel2').appendChild(div).appendChild(img);
                        }
                        else{
                            var div = document.createElement('div');
                            div.className  = "carousel-item";
                            var img = document.createElement('img');
                            img.className  = "d-block w-100";
                            img.src = "../assets/img/" + imagetoJson2[i];;
                            img.alt = "Slide";
                            document.getElementById('gambar-carousel2').appendChild(div).appendChild(img);
                        }
                    }
                }

            }
        });
}

function get_delete_item(obj){
    var rowID = $(obj).attr('data-id');
    console.log(rowID);
    $.ajax({
            url: "../asset/get_info_delete",
            type: "POST",
            data: {
                kode: rowID
            },
            dataType: 'json',
            success: function(data) {
                console.log(data);
                document.getElementById("tgl-delete").innerHTML = data.tanggal_delete;
                document.getElementById("alasan-delete").innerHTML = data.alasan_delete;
            }
        });
}
</script>

