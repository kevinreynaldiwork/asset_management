<script>
    $(document).ready(function(){
        $('#posts').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
            },
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                //panggil method ajax list dengan ajax
                "url": "<?php echo base_url(); ?>auth/fetch_asset_user",
                "type": "POST"
            }
        });
    });

</script>

<br>
<div class="row">
    <div class="col-md-12">
        <h3>DAFTAR PENGGUNA ASSET </h3>
    </div>
</div>
<hr>

<!-- masuk ke content -->
<div class="row">
   
    <div class="col-md-12 mt-2 mb-2">
    <a name="add_users" data-bs-toggle="modal" data-bs-target="#add_users" class="btn btn-dark" role="button" onclick="addUser(this)">Tambah Users</a>
    </div>
    <br>
        <div class="table-responsive">
            <table id="posts" class="table table-striped data-table datatable-ajax" style="width: 100%">
            <thead>
                <tr>
                    <th>NIK</th>
                    <th>Nama</th>
                    <th>Departemen</th>
                    <th>Kategori</th>
                    <th>Nama Asset</th>
                    <th>Kode Asset</th>
                    <th>Location</th>
                    <th>Files</th>
                    <th>Del</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>

    </div>


    <div class="modal fade" id="add_users" tabindex="-1" aria-labelledby="add_users" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title-edit-rumah">Add Users</h5>
                         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="post" action="../auth/add_pengguna_asset" enctype="multipart/form-data" >
                    <div class="container">
                        <h3 style="text-align:center;">Form Add Users </h3>
                        <div class="row">
                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="number" name="nik" style="width:100%;" id="nik"  placeholder="Input Your NIK Here"> <label
                                        style="font-weight:bold; color:black;">NIK</label> </div>
                            </div>

                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="nama"
                                        placeholder="Input Your Name Here" style="width:100%;" id="nama"> <label
                                        style="font-weight:bold; color:black;">Nama</label> </div>
                            </div>
                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="departemen"
                                        placeholder="Input Your Department Here" style="width:100%;" id="departemen"> <label
                                        style="font-weight:bold; color:black;">Departement
                                    </label> </div>
                            </div>

                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">kode</label>
                                <select class="form-select" id="kode_asset" name="kode_asset">
                                  <!-- masukno option disini -->
                                    <!-- <option value="Ada" selected>Ada</option>
                                    <option value="Tidak_ada">Tidak ada</option> -->
                                </select>
                            </div>

                            <div class="col-md-12 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Upload File</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <input type="file" id="file-input-edit" accept="application/pdf, application/vnd.ms-excel" name="userfile[]" multiple>
                                <label for="file-input-edit"><i class="bi bi-upload"></i>Choose File</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <p for="num-of-files-edit" id="num-of-files-edit">No Files Chosen</p>
                            </div>

                        </div>
                        <br>
                        <div style="text-align:center;">
                            <button type="submit" class="btn btn-dark"> Submit </button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- Modal -->
<div class="modal fade" id="add_regist" tabindex="-1" aria-labelledby="add_regist" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="title-perbaikan-asset">Registrasi Form</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
          onclick="window.location.reload();"></button>
      </div>
      <div class="modal-body">
        <form method="post" action="../auth/registrasi">
          <div class="container">
            <h3 style="text-align:center;">Form Registration Username and Password </h3>
            <div class="row">
              <div class="col-md-12 mt-2 mb-2">
                <div class="input-group input1"> <input type="text" name="nik" placeholder="Input Code Here"
                    style="width:100%;" id="asset_code" readonly> <label style="font-weight:bold; color:black;">
                    Code</label> </div>
              </div>
              <div class="col-md-12 mt-2 mb-2">
                <div class="input-group input1"> <input type="text" name="username" placeholder="Input username Here"
                    style="width:100%;"> <label style="font-weight:bold; color:black;">username</label> </div>
              </div>
              <div class="col-md-12 mt-2 mb-2">
                <div class="input-group input1"> <input type="password" name="password"
                    placeholder="Input password Here" style="width:100%;"> <label
                    style="font-weight:bold; color:black;">
                    Password (must more than 5 character)</label> </div>
              </div>

              <div class="col-md-12 mt-2 mb-2">
                <div class="input-group input1"> <input type="password" name="password2"
                    placeholder="Input Confirm Password Here" style="width:100%;"> <label
                    style="font-weight:bold; color:black;">Confirm
                    Password</label> </div>
              </div>

            </div>
            <br>
            <div style="text-align:center;">
              <button type="submit" class="btn btn-dark"> Submit </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="delete_user" tabindex="-1" aria-labelledby="delete_user" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="title-perbaikan-asset">Delete User Form</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
          onclick="window.location.reload();"></button>
      </div>
      <div class="modal-body">
        <form method="post" action="../auth/delete_pengguna">
          <div class="container">
            <h3 style="text-align:center;">Form Delete User </h3>
            <div class="row">
              <div class="col-md-12 mt-2 mb-2">
                <div class="input-group input1"> <input type="text" name="id_user" placeholder="Input Code Here"
                    style="width:100%;" id="delete_code" readonly> <label style="font-weight:bold; color:black;">
                    Code</label> </div>
              </div>
              <div class="col-md-12 mt-2 mb-2">
                <div class="input-group input1"> <input type="text" name="alasan_delete" placeholder="Input Delete Reason Here"
                    style="width:100%;"> <label style="font-weight:bold; color:black;">Delete Reason</label> </div>
              </div>

            </div>
            <br>
            <div style="text-align:center;">
              <button type="submit" class="btn btn-dark"> Submit </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
function showPass() {
  var x = document.getElementById("password");
  var y = document.getElementById("password2");
  if (x.type === "password") {
    x.type = "text";
    y.type = "text";
  } else {
    x.type = "password";
    y.type = "password";
  }
}

function deleteClick(obj){
    var rowID = $(obj).attr('data-id');
    document.getElementById("delete_code").value = rowID;
}

function AddRegist(obj){
    var rowID = $(obj).attr('data-id');
    document.getElementById("asset_code").value = rowID;
}

function addUser(obj){
  $.ajax({
            url: "../asset/get_list_kode_asset",
            type: "POST",
            dataType: 'json',
            success: function(data) {
              console.log(data[0]);
              for(var i = 0; i<data.length;i++){
                if(i == 0){
                  var option = document.createElement('option');
                  option.value = data[i];
                  option.innerHTML = data[i];
                  document.getElementById('kode_asset').appendChild(option);
                }
                else{
                  var option = document.createElement('option');
                  option.value = data[i];
                  option.innerHTML = data[i];
                  document.getElementById('kode_asset').appendChild(option);
                }
              }
            }
          });
}


</script>