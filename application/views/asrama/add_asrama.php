<div class="container-fluid">
            <div class="card border-3 border-dark rounded-1">
                <form method="post" action="<?= base_url() ?>asrama/add_asrama" enctype="multipart/form-data">
                    <div class="container ">
                        <h3 style="text-align:center;">Form menambahkan Asrama</h3>
                        <div class="row">
                            <!-- <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="Asset_Code"
                                        placeholder="Input Asset Code Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Asrama
                                        Code</label> </div>
                            </div>   -->

                            <div class="col-md-12 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Asrama</label>
                                <select class="form-select" id="asrama" name="asrama">
                                    <option value="Glory_Lama" selected>Glory Lama</option>
                                    <option value="Glory_Baru">Glory Baru</option>
                                    <option value="Asrama_26">Asrama 26</option>
                                    <option value="Lebak_Arum">Lebak Arum</option>
                                </select>
                            </div>

                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="number" name="lantai"
                                    placeholder="Input Floor Number" style="width:100%;"> <label
                                    style="font-weight:bold; color:black;">Floor Number </label> </div>
                            </div>
                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="number" name="kamar"
                                    placeholder="Input Room Number" style="width:100%;"> <label
                                    style="font-weight:bold; color:black;">Room Number </label> </div>
                            </div>
                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="number" name="kapasitas"
                                    placeholder="Input Maximum Occupant Here !" style="width:100%;"> <label
                                    style="font-weight:bold; color:black;">Maximum occupant</label> </div>
                            </div>
                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="number" name="penghuni"
                                    placeholder="Input Occupant Here !" style="width:100%;"> <label
                                    style="font-weight:bold; color:black;"> Occupant </label> </div>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="date" name="tgl_pengadaan"
                                        placeholder="Input Procurement Date Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Procurement
                                        Date </label> </div>
                            </div>
                            <div class="fasilitas">
                                <div class="add_fasilitas">
                                    
                                </div>  
                                <div class="tombol_add" style="text-align:center;">
                                    <button type="button" name="fasilitas_button" id="fasilitas_button" class="btn btn-dark">Add more Facility and Number of Rooms </button>
                                </div>
                            </div>
                            
                            <div class="col-md-12 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Upload Photos</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <input type="file" id="file-input" accept="image/png, image/jpeg" onchange="preview()" name="userfile[]" multiple>
                                <label for="file-input"><i class="bi bi-upload"></i>Choose Photo</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <p for="num-of-files" id="num-of-files">No Files Chosen</p>
                                <div id="images"></div>
                            </div>
                        </div>
                        <br>
                        <div style="text-align:center;">
                            <button type="submit" class="btn btn-dark"> Submit </button>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    <script type="text/javascript">
    var i = 1;
    var counter_for_fasilitas  = 1;
    var counter_for_jumlah = 1;
    $("#fasilitas_button").click(function(){
        $(".add_fasilitas").append(
        '<div class="row for_fasilitas">'+
            // Untuk fasilitas
        '<div class="col-md-5 mt-2 mb-2">'+
        '<div class="input-group input1">'+
        '<input type="text" name="fasilitas[]" placeholder="Input Facility Here" id="Facility'+counter_for_fasilitas+'" style="width:100%;">'+
        '<label style="font-weight:bold; color:black;">Facility</label>'+
        '</div>'+
        '</div>'+
        // Untuk jumlah
        '<div class="col-md-5 mt-2 mb-2">'+
        '<div class="input-group input1">'+
        '<input type="text" name="jumlah_fasilitas[]" placeholder="Input Number of room Here" id="JFacility'+counter_for_jumlah+'" style="width:100%;">'+
        '<label style="font-weight:bold; color:black;">Jumlah</label>'+
        '</div>'+
        '</div>'+
        // Button delete
        '<div class="col-md-2" style="margin:auto;">'+
        '<div class="tombol_delete'+counter_for_fasilitas+'" style="text-align:center;">'+
        '<button type="button" onClick="this_delete(this.id)" name="delete_button_'+counter_for_fasilitas+'" id="delete_button_'+counter_for_fasilitas+'" class="btn btn-danger">Delete</button>'+
        '</div>'+
        '</div>'+
        '</div>'
        );
        counter_for_fasilitas +=1;
        counter_for_jumlah +=1;
        });

        function this_delete(clicked) {
                var target = "#"+clicked;
                $(target).parents('.for_fasilitas').remove();
        }         
        // Punyanya gambar input
        let fileInput = document.getElementById("file-input");
        let imageContainer = document.getElementById("images");
        let numOfFiles = document.getElementById("num-of-files");
        function preview(){
            imageContainer.innerHTML = "";
            numOfFiles.textContent = `${fileInput.files.length} Files Selected`;
            for(i of fileInput.files){
                let reader = new FileReader();
                let figure = document.createElement("figure");
                let figCap = document.createElement("figcaption");
                figCap.innerText = i.name;
                figure.appendChild(figCap);
                reader.onload=()=>{
                    let img = document.createElement("img");
                    img.setAttribute("src",reader.result);
                    figure.insertBefore(img,figCap);
                }
                imageContainer.appendChild(figure);
                reader.readAsDataURL(i);
            }


        }

    </script>