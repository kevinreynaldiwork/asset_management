<script>
    $(document).ready(function(){
        $('#posts').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
            },
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                //panggil method ajax list dengan ajax
                "url": "<?php echo base_url(); ?>asrama/fetch_asrama_admin",
                "type": "POST"
            }
        });
    });

</script>
<div class="row">
    <div class="col-md-12">
        <h3>Welcome to Asrama page </h3>
    </div>
</div>
<hr>

<!-- Button Add asset -->
    <div class="col-md-12 mt-2 mb-2">
    <a href="<?= base_url() ?>asrama/add_asrama_form" class="btn btn-dark" role="button" >Tambah Asrama</a>
    </div>
    <!-- Datatable -->
    <div class="col-md-12 mt-2 mb-2">
        <div class="table-responsive">
            <table id="posts" class="table table-striped data-table datatable-ajax" style="width: 100%">
            <thead>
                <tr>
                    <th>Kode Asset</th>
                    <th>Asrama</th>
                    <th>Lantai</th>
                    <th>Kamar</th>
                    <th>Kapasitas</th>
                    <th>Penghuni</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>

    </div>
    <!-- Delete Asset -->
    <div class="modal fade" id="delete_asset" tabindex="-1" aria-labelledby="delete_modal" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Asset</h5>
                         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="post" action="../asrama/delete_asrama">
                        <div class="row">
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="kode" style="width:100%;" id="code_delete" readonly> 
                                <label style="font-weight:bold; color:black;">Code</label> </div>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="alasan_delete"
                                        placeholder="Input Asset Name Here" style="width:100%;" required> <label
                                        style="font-weight:bold; color:black;">Reason
                                        </label> </div>
                            </div>
                        </div>
                        <br>
                        <div style="text-align:center;">
                            <button type="submit" class="btn btn-dark"> Delete </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Untuk perbaikan Asset -->
   <div class="modal fade" id="perbaikan_asset" tabindex="-1" aria-labelledby="perbaikan_asset" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title-perbaikan-asset">Perbaikan Asset</h5>
                         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" onclick="window.location.reload();"></button>
                </div>
                <div class="modal-body">
                    <form method="post" action="../asrama/add_perbaikan" enctype="multipart/form-data">
                    <div class="container">
                        <h3 style="text-align:center;">Form Perbaikan Asset Asrama </h3>
                        <div class="row">
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="kode"
                                        placeholder="Input Code Here" style="width:100%;" id="perbaikanAssetCode" readonly> <label
                                        style="font-weight:bold; color:black;">
                                        Code</label> </div>
                            </div>  
                        <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="date" name="tgl_kejadian"
                                        placeholder="Input Procurement Date Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Incident
                                        Date </label> </div>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="kronologi"
                                        placeholder="Input Asset Code Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">
                                        Chronology</label> </div>
                            </div>

                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="kondisi"
                                        placeholder="Input Asset Name Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Asset
                                        Condition</label> </div>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="action_plan"
                                        placeholder="Input Asset Name Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Action
                                        Plan</label> </div>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="RAB"
                                        placeholder="Input Asset Name Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">
                                        RAB</label> </div>
                            </div>

                            <div class="col-md-12 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Upload Photos</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <input type="file" id="file-input" accept="image/png, image/jpeg" onchange="preview()" name="userfile[]" multiple>
                                <label for="file-input"><i class="bi bi-upload"></i>Choose Photo</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <p for="num-of-files" id="num-of-files">No Files Chosen</p>
                                <div id="images"></div>
                            </div>

                        </div>
                        <br>
                        <div style="text-align:center;">
                            <button type="submit" class="btn btn-dark"> Submit </button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

     <!-- Edit Asrama  -->
     <div class="modal fade" id="edit_asrama" tabindex="-1" aria-labelledby="edit_asrama" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Fasilitas</h5>
                         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="post" action="../asrama/edit_asrama" enctype="multipart/form-data">
                    <div class="row">
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="kode"
                                        placeholder="Input Asset Code Here" style="width:100%;" id="asset_code_edit"> <label
                                        style="font-weight:bold; color:black;">Asrama
                                        Code</label> </div>
                            </div>  
                            <div class="col-md-12 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Asrama</label>
                                <select class="form-select" id="nama_edit" name="asrama">
                                    <option value="Glory_Lama" selected>Glory Lama</option>
                                    <option value="Glory_Baru">Glory Baru</option>
                                    <option value="Asrama_26">Asrama 26</option>
                                    <option value="Lebak_Arum">Lebak Arum</option>
                                </select>
                            </div>

                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="number" name="lantai"
                                    placeholder="Input Floor Number" style="width:100%;" id="lantai_edit"> <label
                                    style="font-weight:bold; color:black;">Floor Number </label> </div>
                            </div>
                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="number" name="kamar"
                                    placeholder="Input Room Number" style="width:100%;" id="kamar_edit"> <label
                                    style="font-weight:bold; color:black;">Room Number </label> </div>
                            </div>
                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="number" name="kapasitas"
                                    placeholder="Input Maximum Occupant Here !" style="width:100%;" id="kapasitas_edit"> <label
                                    style="font-weight:bold; color:black;">Maximum occupant</label> </div>
                            </div>
                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="number" name="penghuni"
                                    placeholder="Input Occupant Here !" style="width:100%;" id="penghuni_edit"> <label
                                    style="font-weight:bold; color:black;"> Occupant </label> </div>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="date" name="tgl_pengadaan"
                                        placeholder="Input Procurement Date Here" style="width:100%;" id="tgl_pengadaan_edit"> <label
                                        style="font-weight:bold; color:black;">Procurement
                                        Date </label> </div>
                            </div>
                            <div class="fasilitas">
                                <div class="add_fasilitas">
                                    
                                </div>  
                                <div class="tombol_add" style="text-align:center;">
                                    <button type="button" name="fasilitas_button" id="fasilitas_button" class="btn btn-dark">Add more Facility and Number of Rooms </button>
                                </div>
                            </div>
                            
                            <div class="col-md-12 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Upload Photos</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <input type="file" id="file-input-edit" accept="image/png, image/jpeg" onchange="preview2()" name="userfile[]" multiple>
                                <label for="file-input-edit"><i class="bi bi-upload"></i>Choose Photo</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <p for="num-of-files-edit" id="num-of-files-edit">No Files Chosen</p>
                                <div id="images-edit"></div>
                            </div>
                        </div>
                        <br>
                        <div style="text-align:center;">
                            <button type="submit" class="btn btn-dark"> Submit </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    

</div>
<!-- Datatable -->
<script>
var counter_for_fasilitas = 1; 
var counter_for_jumlah = 1;
$('#edit_asrama').on('hidden.bs.modal', function () {
    location.reload();
})
$('#perbaikan_asset').on('hidden.bs.modal', function () {
    location.reload();
})
$('#delete_asset').on('hidden.bs.modal', function () {
    location.reload();
})
function deleteClick (obj) {
      var rowID = $(obj).attr('data-id');
      document.getElementById("code_delete").value = rowID;
}
function this_delete(clicked) {
        var target = "#"+clicked;
        $(target).parents('.for_fasilitas').remove();
}      

$("#fasilitas_button").click(function(){
        $(".add_fasilitas").append(
        '<div class="row for_fasilitas">'+
            // Untuk fasilitas
        '<div class="col-md-5 mt-2 mb-2">'+
        '<div class="input-group input1">'+
        '<input type="text" name="fasilitas[]" placeholder="Input Facility Here" id="Facility'+counter_for_fasilitas+'" style="width:100%;">'+
        '<label style="font-weight:bold; color:black;">Facility</label>'+
        '</div>'+
        '</div>'+
        // Untuk jumlah
        '<div class="col-md-5 mt-2 mb-2">'+
        '<div class="input-group input1">'+
        '<input type="text" name="jumlah_fasilitas[]" placeholder="Input Number of room Here" id="JFacility'+counter_for_jumlah+'" style="width:100%;">'+
        '<label style="font-weight:bold; color:black;">Jumlah</label>'+
        '</div>'+
        '</div>'+
        // Button delete
        '<div class="col-md-2" style="margin:auto;">'+
        '<div class="tombol_delete'+counter_for_fasilitas+'" style="text-align:center;">'+
        '<button type="button" onClick="this_delete(this.id)" name="delete_button_'+counter_for_fasilitas+'" id="delete_button_'+counter_for_fasilitas+'" class="btn btn-danger">Delete</button>'+
        '</div>'+
        '</div>'+
        '</div>'
        );
        counter_for_fasilitas +=1;
        counter_for_jumlah +=1;
        });

function editAsrama(obj){
    var rowID = $(obj).attr('data-id');
    document.getElementById("asset_code_edit").value = rowID;
    $.ajax({
            url: "../asrama/get_asrama_json",
            type: "POST",
            data: {
                kode: rowID
            },
            dataType: 'json',
            success: function(data) {
                console.log(data);
                var coba = data.info_asset;
                var obj = JSON.parse(coba);
                console.log(obj);
                document.getElementById("nama_edit").value = data.nama;
                document.getElementById("lantai_edit").value = obj.lantai;
                document.getElementById("kamar_edit").value = obj.kamar;
                document.getElementById("kapasitas_edit").value = obj.kapasitas;
                document.getElementById("penghuni_edit").value = obj.penghuni;
                document.getElementById("tgl_pengadaan_edit").value = data.tanggal_terima;
                var fasilitas = data.fasilitas;
                var fasilitasJSON = JSON.parse(fasilitas);
                console.log(fasilitasJSON);
                var jumlah_fasilitas = data.jumlah_fasilitas;
                var jumlah_fasilitasJSON = JSON.parse(jumlah_fasilitas);
                console.log(jumlah_fasilitasJSON);
                for(var i = 0; i<jumlah_fasilitasJSON.length; i++){
                    $(".add_fasilitas").append(
                        '<div class="row for_fasilitas">'+
                        // Untuk fasilitas
                        '<div class="col-md-5 mt-2 mb-2">'+
                        '<div class="input-group input1">'+
                        '<input type="text" name="fasilitas[]" placeholder="Input Facility Here" id="Facility'+counter_for_fasilitas+'" style="width:100%;">'+
                        '<label style="font-weight:bold; color:black;">Facility</label>'+
                        '</div>'+
                        '</div>'+
                        // Untuk jumlah
                        '<div class="col-md-5 mt-2 mb-2">'+
                        '<div class="input-group input1">'+
                        '<input type="text" name="jumlah_fasilitas[]" placeholder="Input Number of room Here" id="JFacility'+counter_for_jumlah+'" style="width:100%;">'+
                        '<label style="font-weight:bold; color:black;">Jumlah</label>'+
                        '</div>'+
                        '</div>'+
                        // Button delete
                        '<div class="col-md-2" style="margin:auto;">'+
                        '<div class="tombol_delete'+counter_for_fasilitas+'" style="text-align:center;">'+
                        '<button type="button" onClick="this_delete(this.id)" name="delete_button_'+counter_for_fasilitas+'" id="delete_button_'+counter_for_fasilitas+'" class="btn btn-danger">Delete</button>'+
                        '</div>'+
                        '</div>'+
                        '</div>'
                    );
                    document.getElementById("Facility"+counter_for_fasilitas).value = fasilitasJSON[counter_for_fasilitas-1];
                    document.getElementById("JFacility"+counter_for_jumlah).value = jumlah_fasilitasJSON[counter_for_jumlah-1];
                    counter_for_fasilitas += 1; 
                    counter_for_jumlah += 1;
                }
                
                // untuk foto
                var getnamepict = data.foto_asset;
                var imagetoJson = JSON.parse(getnamepict);
                if(imagetoJson[0] !=""){
                    let numOfFiles = document.getElementById("num-of-files-edit");
                    numOfFiles.textContent = `${imagetoJson.length} Files Selected`;
                    for(var i = 0; i< imagetoJson.length; i++){
                        var img = document.createElement('img')
                        img.src = "../assets/img/"+imagetoJson[i];
                        img.style = "width:149px;height:101px;";
                        document.getElementById('images-edit').appendChild(img);
                        var nickname = document.createElement('p');
                        nickname.style="font-size:10px;margin-left:35px;"
                        var node = document.createTextNode(imagetoJson[i]);
                        nickname.appendChild(node);
                        document.getElementById('images-edit').appendChild(nickname);
                    }   
                }
                // jgn lupa di decode
            }
        });
}

function perbaikanAsset(obj){
    var rowID = $(obj).attr('data-id');
    document.getElementById("perbaikanAssetCode").value = rowID;
}

function preview(){
        let fileInput = document.getElementById("file-input");
        let imageContainer = document.getElementById("images");
        let numOfFiles = document.getElementById("num-of-files");
        imageContainer.innerHTML = "";
        numOfFiles.textContent = `${fileInput.files.length} Files Selected`;
        for(i of fileInput.files){
            let reader = new FileReader();
            let figure = document.createElement("figure");
            let figCap = document.createElement("figcaption");
            figCap.innerText = i.name;
            figure.appendChild(figCap);
            reader.onload=()=>{
                let img = document.createElement("img");
                img.setAttribute("src",reader.result);
                figure.insertBefore(img,figCap);
             }
            imageContainer.appendChild(figure);
            reader.readAsDataURL(i);
            }


}

function preview2(){
        let fileInput = document.getElementById("file-input-edit");
        let imageContainer = document.getElementById("images-edit");
        let numOfFiles = document.getElementById("num-of-files-edit");
        imageContainer.innerHTML = "";
        numOfFiles.textContent = `${fileInput.files.length} Files Selected`;
        for(i of fileInput.files){
            let reader = new FileReader();
            let figure = document.createElement("figure");
            let figCap = document.createElement("figcaption");
            figCap.innerText = i.name;
            figure.appendChild(figCap);
            reader.onload=()=>{
                let img = document.createElement("img");
                img.setAttribute("src",reader.result);
                figure.insertBefore(img,figCap);
             }
            imageContainer.appendChild(figure);
            reader.readAsDataURL(i);
            }
}
</script>
