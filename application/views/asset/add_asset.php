<div class="container-fluid">
            <div class="card border-3 border-dark rounded-1">
                <form action="">
                    <div class="container ">
                        <h3 style="text-align:center;">Form menambahkan asset</h3>
                        <div class="row">
                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="Asset_Code"
                                        placeholder="Input Asset Code Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Asset
                                        Code</label> </div>
                            </div>

                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="Asset_Name"
                                        placeholder="Input Asset Name Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Asset
                                        Name</label> </div>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="Location"
                                        placeholder="Input Location Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Location
                                    </label> </div>
                            </div>

                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="date" name="Tanggal_Pengadaan"
                                        placeholder="Input Procurement Date Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Procurement
                                        Date </label> </div>
                            </div>
                            <div class="col-md-6 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Asset Type</label>
                                <select class="form-select" id="Asset_Type">
                                    <option value="Tetap" selected>Tetap</option>
                                    <option value="bergerak">Bergerak</option>
                                </select>
                            </div>

                            <div class="col-md-6 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Precondition</label>
                                <select class="form-select" id="Precondition">
                                    <option value="Baik" selected>Baik</option>
                                    <option value="rusak">Rusak</option>
                                </select>
                            </div>

                            <div class="col-md-4 mt-2 mb-2">
                                <div class="input-group input1"> <input type="number" name="Bedroom"
                                    placeholder="Input Number of Rooms Here" style="width:100%;"> <label
                                    style="font-weight:bold; color:black;">Number of Rooms </label> </div>
                            </div>

                            
                            <div class="col-md-4 mt-2 mb-2">
                                <div class="input-group input1"> <input type="number" name="Bathroom"
                                    placeholder="Input Number of Bathrooms Here" style="width:100%;"> <label
                                    style="font-weight:bold; color:black;">Number of Bathrooms </label> </div>
                            </div>

                            <div class="col-md-4 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Carport</label>
                                <select class="form-select" id="Carport">
                                    <option value="Ada" selected>Ada</option>
                                    <option value="Tidak_ada">Tidak ada</option>
                                </select>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Upload Photos</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <input type="file" id="file-input" accept="image/png, image/jpeg" onchange="preview()" multiple>
                                <label for="file-input"><i class="bi bi-upload"></i>Choose Photo</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <p for="num-of-files" id="num-of-files">No Files Chosen</p>
                                <div id="images"></div>
                            </div>

                        </div>
                    </div>

                </form>
            </div>

        </div>