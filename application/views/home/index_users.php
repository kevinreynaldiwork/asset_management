<div class="card bg-dark text-white">
  <img class="card-img" src="../assets/img/Bg/bg1.jpg" alt="Card image" style="height:30rem;">
  <div class="card-img-overlay">
    <div class="body-text2">
      <h3>Welcome to !</h3>
      <br>
      <h1>Asset</h1>
      <br>
      <h1>Management</h1>
      <br>
      <h3>by Human Resource Department</h3>
      <br>
      <div style="padding-bottom:100px;"></div>
    </div>
  </div>
</div>

<div class="row nopadding">
  <div class="col-md-6 nopadding">
    <div class="card bg-dark text-white">
      <a href="<?= base_url(); ?>/rumah/index_users">
        <img class="card-img" src="../assets/img/Bg/bg2.jpg" alt="Card image" style="height:20rem;">
        <div class="card-img-overlay">
          <div class="body-text2">
            <br>
            <br>
            <h3>Go to Rumah Dinas Page </h3>
          </div>
        </div>
      </a>
    </div>
  </div>
  <!-- Pembagi -->
  <div class="col-md-6 nopadding">
    <div class="card bg-dark text-white">
      <a href="<?= base_url(); ?>/gedung/index_users">
        <img class="card-img" src="../assets/img/Bg/bg5.jpg" alt="Card image" style="height:20rem;">
        <div class="card-img-overlay">
          <div class="body-text2">
            <br>
            <br>
            <h3>Go to Gedung Page </h3>
          </div>
        </div>
      </a>
    </div>
  </div>
</div>

<div class="row nopadding">
    <div class="col-md-4 nopadding">
    <div class="card bg-dark text-white">
      <a href="<?= base_url(); ?>/kendaraan/index_users">
        <img class="card-img" src="../assets/img/Bg/bg4.jpg" alt="Card image" style="height:20rem;">
        <div class="card-img-overlay">
          <div class="body-text2">
            <br>
            <br>
            <h3>Go to Kendaraan Page </h3>
          </div>
        </div>
      </a>
    </div>
    </div>
    <div class="col-md-4 nopadding">
    <div class="card bg-dark text-white">
      <a href="<?= base_url(); ?>/rumah/index_users">
        <img class="card-img" src="../assets/img/Bg/bg6.jpg" alt="Card image" style="height:20rem;">
        <div class="card-img-overlay">
          <div class="body-text2">
             <br>
             <br>
            <h3>Go to Asrama Page </h3>
          </div>
        </div>
      </a>
    </div>
    </div>
    <div class="col-md-4 nopadding">
    <div class="card bg-dark text-white">
      <a href="<?= base_url(); ?>/furnitur/index_users">
        <img class="card-img" src="../assets/img/Bg/bg7.jpg" alt="Card image" style="height:20rem;">
        <div class="card-img-overlay">
          <div class="body-text2">
          <br>
          <br>
            <h3>Go to Fasilitas Page </h3>
          </div>
        </div>
      </a>
    </div>
    </div>
</div>