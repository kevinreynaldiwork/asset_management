<!doctype html>
<html lang="en">
  <head>
  	<title>Admin Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="<?= base_url(); ?>/assets/css/style_login.css" />

	</head>
	<body>
	<section class="ftco-section">
		<div class="container">
			<!-- <div class="row justify-content-center">
				<div class="col-md-6 text-center mb-5">
					<h2 class="heading-section">Login For Admin</h2>
				</div>
			</div> -->
			<div class="row justify-content-center">
				<div class="col-md-6 col-lg-5">
					<div class="login-wrap p-4 p-md-5">
		      	<div class="icon d-flex align-items-center justify-content-center">
		      		<span class="fa fa-user-o"></span>
		      	</div>
		      	<h3 class="text-center mb-4">Login Here!</h3>
						<form method="post" class="login-form"  action="<?= base_url(); ?>auth/login">
		      		<div class="form-group">
		      			<input type="text" class="form-control rounded-left" placeholder="Username" id="username" name="username"
						value=" <?= set_value('username'); ?>">
		      			<?= form_error('username', '<small class="text-danger pl-3">','</small>'); ?>
					</div>
	            <div class="form-group">
	              <input type="password" class="form-control rounded-left" placeholder="Password" id="password" name="password">
				  <?= form_error('password', '<small class="text-danger pl-3">','</small>'); ?>
				</div>
                <div class="w-50">
                        <input type="checkbox" onclick="showPass()"> Show Password
                </div>
	            <div class="form-group">
	            	<button type="submit" class="btn btn-primary rounded submit p-3 px-5">Get Started</button>
	            </div>
	          </form>
	        </div>
				</div>
			</div>
		</div>
	</section>
	<script>
function showPass() {
  var x = document.getElementById("password");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
</script>

	<script src="<?= base_url(); ?>/assets/js/jquery.min.js"></script>
  <script src="<?= base_url(); ?>/assets/js/popper.js"></script>
  <script src="<?= base_url(); ?>/assets/js/bootstrap.min.js"></script>
  <script src="<?= base_url(); ?>/assets/js/main.js"></script>
	</body>
</html>

