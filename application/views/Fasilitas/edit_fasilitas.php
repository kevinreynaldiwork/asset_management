<div class="container-fluid">
            <div class="card border-3 border-dark rounded-1">
                <form method="post" action="<?= base_url() ?>fasilitas/edit_fasilitas" enctype="multipart/form-data">
                    <div class="container">
                        <h3 style="text-align:center;">Form menambahkan fasilitas</h3>
                        <div class="row">
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="nama"
                                        placeholder="Input Asset Code Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Asset
                                        Name</label> </div>
                            </div>

                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="kode"
                                        placeholder="Input Asset Name Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Asset
                                        Code</label> </div>
                            </div>
                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="lokasi"
                                        placeholder="Input Location Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Location
                                    </label> </div>
                            </div>

                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="date" name="tgl_pengadaan"
                                        placeholder="Input Procurement Date Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Procurement
                                        Date </label> </div>
                            </div>
                            <div class="col-md-6 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Asset Type</label>
                                <select class="form-select" id="jenis" name="jenis">
                                    <option value="Tetap" selected>Tetap</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div>

                            <div class="col-md-6 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Precondition</label>
                                <select class="form-select" id="kondisi" name="kondisi">
                                    <option value="Baik" selected>Baik</option>
                                    <option value="Rusak">Rusak</option>
                                    <option value="3">Three</option>
                                </select>
                            </div>

                            <div class="col-md-12 mt-2 mb-2">
                            <div class="input-group input1"> <input type="number" name="garansi"
                                        placeholder="Input Garanty Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Garanty
                                    </label> </div>
                            </div>

                            
                            <div class="col-md-12 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Upload Photos</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <input type="file" id="file-input" accept="image/png, image/jpeg" onchange="preview()" name="userfile[]" multiple>
                                <label for="file-input"><i class="bi bi-upload"></i>Choose Photo</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <p for="num-of-files" id="num-of-files">No Files Chosen</p>
                                <div id="images"></div>
                            </div>

                        </div>
                        <br>
                        <div style="text-align:center;">
                            <button type="submit" class="btn btn-dark"> Submit </button>
                        </div>
                    </div>

                </form>
                <br>
            </div>

        </div>

        <script>
        let fileInput = document.getElementById("file-input");
        let imageContainer = document.getElementById("images");
        let numOfFiles = document.getElementById("num-of-files");
        function preview(){
            imageContainer.innerHTML = "";
            numOfFiles.textContent = `${fileInput.files.length} Files Selected`;
            for(i of fileInput.files){
                let reader = new FileReader();
                let figure = document.createElement("figure");
                let figCap = document.createElement("figcaption");
                figCap.innerText = i.name;
                figure.appendChild(figCap);
                reader.onload=()=>{
                    let img = document.createElement("img");
                    img.setAttribute("src",reader.result);
                    figure.insertBefore(img,figCap);
                }
                imageContainer.appendChild(figure);
                reader.readAsDataURL(i);
            }


        }
        </script>