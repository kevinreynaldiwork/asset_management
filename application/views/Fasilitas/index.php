<script>
    $(document).ready(function(){
        $('#posts').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
            },
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                //panggil method ajax list dengan ajax
                "url": "<?php echo base_url(); ?>fasilitas/fetch_fasilitas_admin",
                "type": "POST"
            }
        });
    });

</script>

<div class="row">
    <div class="col-md-12">
        <h3>Selamat datang di halaman fasilitas</h3>
</div>
<hr>
<!-- Button Add asset -->
    <div class="col-md-12 mt-2 mb-2">
    <a href="<?= base_url() ?>fasilitas/add_fasilitas_form" class="btn btn-dark" role="button" >Tambah Fasilitas</a>
    </div>
    <!-- Datatable -->
    <div class="col-md-12 mt-2 mb-2">
        <div class="table-responsive">
            <table id="posts" class="table table-striped data-table datatable-ajax" style="width: 100%">
            <thead>
                <tr>
                    <th>Kode Asset</th>
                    <th>Nama Asset</th>
                    <th>Lokasi</th>
                    <th>Tanggal</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>

    </div>
    <!-- Delete asset for fasilitas -->
    <div class="modal fade" id="delete_asset" tabindex="-1" aria-labelledby="delete_modal" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Asset</h5>
                         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="post" action="../fasilitas/delete_fasilitas">
                        <div class="row">
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="kode" style="width:100%;" id="code_delete" readonly> 
                                <label style="font-weight:bold; color:black;">Code</label> </div>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="alasan_delete"
                                        placeholder="Input Asset Name Here" style="width:100%;" required> <label
                                        style="font-weight:bold; color:black;">Reason
                                        </label> </div>
                            </div>
                        </div>
                        <br>
                        <div style="text-align:center;">
                            <button type="submit" class="btn btn-dark"> Delete </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

   <!-- Modal Untuk perbaikan Asset -->
   <div class="modal fade" id="perbaikan_asset" tabindex="-1" aria-labelledby="perbaikan_asset" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title-perbaikan-asset">Perbaikan Asset</h5>
                         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" onclick="window.location.reload();"></button>
                </div>
                <div class="modal-body">
                    <form method="post" action="../fasilitas/add_perbaikan" enctype="multipart/form-data">
                    <div class="container">
                        <h3 style="text-align:center;">Form Perbaikan Asset Fasilitas </h3>
                        <div class="row">
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="kode"
                                        placeholder="Input Code Here" style="width:100%;" id="perbaikanAssetCode" readonly> <label
                                        style="font-weight:bold; color:black;">
                                        Code</label> </div>
                            </div>  
                        <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="date" name="tgl_kejadian"
                                        placeholder="Input Procurement Date Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Incident
                                        Date </label> </div>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="kronologi"
                                        placeholder="Input Asset Code Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">
                                        Chronology</label> </div>
                            </div>

                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="kondisi"
                                        placeholder="Input Asset Name Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Asset
                                        Condition</label> </div>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="action_plan"
                                        placeholder="Input Asset Name Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Action
                                        Plan</label> </div>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="RAB"
                                        placeholder="Input Asset Name Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">
                                        RAB</label> </div>
                            </div>

                            <div class="col-md-12 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Upload Photos</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <input type="file" id="file-input" accept="image/png, image/jpeg" onchange="preview()" name="userfile[]" multiple>
                                <label for="file-input"><i class="bi bi-upload"></i>Choose Photo</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <p for="num-of-files" id="num-of-files">No Files Chosen</p>
                                <div id="images"></div>
                            </div>

                        </div>
                        <br>
                        <div style="text-align:center;">
                            <button type="submit" class="btn btn-dark"> Submit </button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Edit fasilitas -->
    <div class="modal fade" id="edit_fasilitas" tabindex="-1" aria-labelledby="edit_fasilitas" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Fasilitas</h5>
                         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="post" action="../fasilitas/edit_fasilitas" enctype="multipart/form-data">
                    <div class="row">
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="kode"
                                        placeholder="Input Asset Name Here" style="width:100%;" id="asset_code_edit" readonly> <label
                                        style="font-weight:bold; color:black;">Asset
                                        Code</label> </div>
                            </div>
                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="nama"
                                        placeholder="Input Asset Code Here" style="width:100%;" id="asset_name_edit"> <label
                                        style="font-weight:bold; color:black;">Asset
                                        Name</label> </div>
                            </div>
                            <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="lokasi"
                                        placeholder="Input Location Here" style="width:100%;" id = "lokasi_edit"> <label
                                        style="font-weight:bold; color:black;">Location
                                    </label> </div>
                            </div>

                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="date" name="tgl_pengadaan"
                                        placeholder="Input Procurement Date Here" style="width:100%;" id="tgl_pengadaan_edit"> <label
                                        style="font-weight:bold; color:black;">Procurement
                                        Date </label> </div>
                            </div>
                            <div class="col-md-6 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Asset Type</label>
                                <select class="form-select" id="jenis_edit" name="jenis">
                                    <option value="Tetap" selected>Tetap</option>
                                    <option value="bergerak">Bergerak</option>
                                </select>
                            </div>

                            <div class="col-md-6 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Precondition</label>
                                <select class="form-select" id="kondisi_edit" name="kondisi">
                                    <option value="Baik" selected>Baik</option>
                                    <option value="Rusak">Rusak</option>
                                </select>
                            </div>

                            <div class="col-md-12 mt-2 mb-2">
                            <div class="input-group input1"> <input type="text" name="garansi"
                                        placeholder="Input Garanty Here" style="width:100%;" id="garansi_edit"> <label
                                        style="font-weight:bold; color:black;">Garanty
                                    </label> </div>
                            </div>

                            
                            <div class="col-md-12 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Upload Photos</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <input type="file" id="file-input-edit" accept="image/png, image/jpeg" onchange="preview2()" name="userfile[]" multiple>
                                <label for="file-input-edit"><i class="bi bi-upload"></i>Choose Photo</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <p for="num-of-files-edit" id="num-of-files-edit">No Files Chosen</p>
                                <div id="images-edit"></div>
                            </div>

                        </div>
                        <br>
                        <div style="text-align:center;">
                            <button type="submit" class="btn btn-dark"> Submit </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- Datatable -->

<script>
$('#edit_fasilitas').on('hidden.bs.modal', function () {
    location.reload();
})
$('#perbaikan_asset').on('hidden.bs.modal', function () {
    location.reload();
})
$('#delete_asset').on('hidden.bs.modal', function () {
    location.reload();
})
function deleteClick (obj) {
      var rowID = $(obj).attr('data-id');
      document.getElementById("code_delete").value = rowID;
}

function editFasilitas(obj){
    var rowID = $(obj).attr('data-id');
    document.getElementById("asset_code_edit").value = rowID;
    $.ajax({
            url: "../fasilitas/get_fasilitas_json",
            type: "POST",
            data: {
                kode: rowID
            },
            dataType: 'json',
            success: function(data) {
                console.log(data);
                var coba = data.info_asset;
                var obj = JSON.parse(coba);
                console.log(obj);
                document.getElementById("asset_name_edit").value = data.nama;
                document.getElementById("lokasi_edit").value = data.lokasi;
                document.getElementById("tgl_pengadaan_edit").value = data.tanggal_terima;
                document.getElementById("jenis_edit").value = obj.jenis;
                document.getElementById("kondisi_edit").value = obj.kondisi;
                document.getElementById("garansi_edit").value = obj.garansi;
                var getnamepict = data.foto_asset;
                var imagetoJson = JSON.parse(getnamepict);
                if(imagetoJson[0] !=""){
                    let numOfFiles = document.getElementById("num-of-files-edit");
                    numOfFiles.textContent = `${imagetoJson.length} Files Selected`;
                    for(var i = 0; i< imagetoJson.length; i++){
                        var img = document.createElement('img')
                        img.src = "../assets/img/"+imagetoJson[i];
                        img.style = "width:149px;height:101px;";
                        document.getElementById('images-edit').appendChild(img);
                        var nickname = document.createElement('p');
                        nickname.style="font-size:10px;margin-left:35px;"
                        var node = document.createTextNode(imagetoJson[i]);
                        nickname.appendChild(node);
                        document.getElementById('images-edit').appendChild(nickname);
                    }   
                }
                // jgn lupa di decode
            }
        });
}

function perbaikanAsset(obj){
    var rowID = $(obj).attr('data-id');
    document.getElementById("perbaikanAssetCode").value = rowID;
}

function preview(){
        let fileInput = document.getElementById("file-input");
        let imageContainer = document.getElementById("images");
        let numOfFiles = document.getElementById("num-of-files");
        imageContainer.innerHTML = "";
        numOfFiles.textContent = `${fileInput.files.length} Files Selected`;
        for(i of fileInput.files){
            let reader = new FileReader();
            let figure = document.createElement("figure");
            let figCap = document.createElement("figcaption");
            figCap.innerText = i.name;
            figure.appendChild(figCap);
            reader.onload=()=>{
                let img = document.createElement("img");
                img.setAttribute("src",reader.result);
                figure.insertBefore(img,figCap);
             }
            imageContainer.appendChild(figure);
            reader.readAsDataURL(i);
            }


}

function preview2(){
        let fileInput = document.getElementById("file-input-edit");
        let imageContainer = document.getElementById("images-edit");
        let numOfFiles = document.getElementById("num-of-files-edit");
        imageContainer.innerHTML = "";
        numOfFiles.textContent = `${fileInput.files.length} Files Selected`;
        for(i of fileInput.files){
            let reader = new FileReader();
            let figure = document.createElement("figure");
            let figCap = document.createElement("figcaption");
            figCap.innerText = i.name;
            figure.appendChild(figCap);
            reader.onload=()=>{
                let img = document.createElement("img");
                img.setAttribute("src",reader.result);
                figure.insertBefore(img,figCap);
             }
            imageContainer.appendChild(figure);
            reader.readAsDataURL(i);
            }
}
</script>
