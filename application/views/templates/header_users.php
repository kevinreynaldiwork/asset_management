<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet" />
    <link rel="icon"  href="<?= base_url(); ?>assets/images/icons/favicon.ico"/>
	<link rel="stylesheet" href="<?= base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/styles2.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/dataTables.bootstrap5.min.css" />
    <script src="<?= base_url(); ?>/assets/js/jquery-3.5.1.js"></script>
    <title>Asset Management</title>
</head>

<body>
    <nav>
        <div class="heading">
            <h4>Asset Management</h4>
        </div>
        <ul class="nav-links">
            <li><a href="../home/user">Home</a></li>
            <li><a href="../rumah/index_users">Rumah Dinas</a></li>
            <li><a href="../gedung/index_users">Gedung</a></li>
            <li><a href="../kendaraan/index_users">Kendaraan</a></li>
            <li><a href="../asrama/index_users">Asrama</a></li>
            <li><a href="../fasilitas/index_users">Fasilitas</a></li>
            <ul><a href="../auth/logout">Logout</a></ul>
        </ul>
    </nav>