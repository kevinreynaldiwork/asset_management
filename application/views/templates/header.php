<!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" />
        <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/dataTables.bootstrap5.min.css" />
        <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/style.css" />
        <script src="<?= base_url(); ?>/assets/js/jquery-3.5.1.js"></script>
        <title>Asset Management</title>
    </head>

    <body>

        <!-- Navbar  -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container-fluid">
                <div>
                    <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#sidebar"
                        aria-controls="offcanvasExample">
                        <span class="navbar-toggler-icon" data-bs-target="#sidebar"></span>
                    </button>
                    <a class="navbar-brand me-auto ms-lg-0 ms-3 text-uppercase fw-bold" href="#">Asset Management</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#topNavBar"
                        aria-controls="topNavBar" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="topNavBar">
                    <ul class="navbar-nav d-flex ms-auto my-3 my-lg-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle ms-2" href="#" role="button" data-bs-toggle="dropdown"
                                aria-expanded="false">
                                <i class="bi bi-person-fill"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end">
                                <li><a class="dropdown-item" href="#">Profil</a></li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                                <li>
                                    <a class="dropdown-item" href="<?= base_url(); ?>auth/logout">Logout</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Navbar -->

        <!-- Offcanvas -->
        <div class="offcanvas offcanvas-start sidebar-nav bg-dark" tabindex="-1" id="sidebar">
            <div class="offcanvas-body p-0">
                <nav class="navbar-dark">
                    <ul class="navbar-nav">
                        <li class="my-2">
                            <!-- pembatas -->
                        </li>
                        <li>
                            <form class="nav-link px-3">
                                <div class="input-group">
                                    <input class="form-control" type="search" placeholder="Search" aria-label="Search" />
                                    <button class="btn btn-outline-success" type="submit">
                                        <i class="bi bi-search"></i>
                                    </button>
                                </div>
                            </form>
                        </li>
                        <li class="my-2">
                            <!-- pembatas -->
                            <hr class="dropdown-divider bg-light" />
                        </li>
                        <li>
                            <a href="<?= base_url(); ?>home/index" class="nav-link px-3 active">
                                <span class="me-2"><i class="bi bi-speedometer2"></i></span>
                                <span>Main Dashboard</span>
                            </a>
                        </li>
                        <li class="my-1">
                            <!-- pembatas -->
                            <hr class="dropdown-divider bg-light" />
                        </li>
                        <li>
                            <a href="<?= base_url(); ?>rumah/index" class="nav-link px-3 active">
                                <span class="me-2"><i class="bi bi-table"></i></span>
                                <span>Table Rumah Dinas</span>
                            </a>
                        </li>
                        <li class="my-1">
                            <!-- pembatas -->
                            <hr class="dropdown-divider bg-light" />
                        </li>
                        <li>
                            <a href="<?= base_url(); ?>gedung/index" class="nav-link px-3 active">
                                <span class="me-2"><i class="bi bi-table"></i></span>
                                <span>Table Gedung</span>
                            </a>
                        </li>
                        <li class="my-1">
                            <!-- pembatas -->
                            <hr class="dropdown-divider bg-light" />
                        </li>
                        <li>
                            <a href="<?= base_url(); ?>kendaraan/index" class="nav-link px-3 active">
                                <span class="me-2"><i class="bi bi-table"></i></span>
                                <span>Table Kendaraan</span>
                            </a>
                        </li>
                        <li class="my-1">
                            <!-- pembatas -->
                            <hr class="dropdown-divider bg-light" />
                        </li>
                        <li>
                            <a href="<?= base_url(); ?>asrama/index" class="nav-link px-3 active">
                                <span class="me-2"><i class="bi bi-table"></i></span>
                                <span>Table Asrama</span>
                            </a>
                        </li>
                        <li class="my-1">
                            <!-- pembatas -->
                            <hr class="dropdown-divider bg-light" />
                        </li>
                        <li>
                            <a href="<?= base_url(); ?>fasilitas/index" class="nav-link px-3 active">
                                <span class="me-2"><i class="bi bi-table"></i></span>
                                <span>Table Fasilitas</span>
                            </a>
                        </li>
                        <li class="my-1">
                            <!-- pembatas -->
                            <hr class="dropdown-divider bg-light" />
                        </li>
                        <li>
                            <a href="<?= base_url(); ?>auth/index" class="nav-link px-3 active">
                                <span class="me-2"><i class="bi bi-people"></i></span>
                                <span>Users</span>
                            </a>
                        </li>
                        <li class="my-1">
                            <!-- pembatas -->
                            <hr class="dropdown-divider bg-light" />
                        </li>
                    </ul>
                </nav>
            </div>
        </div>


        <!-- Offcanvas -->
        <main class="mt-5 pt-3">
          <div class="container">