<div class="container-fluid">
            <div class="card border-3 border-dark rounded-1">
                <form method="post" action="<?= base_url() ?>rumah/add_rumah" enctype="multipart/form-data">
                    <div class="container">
                        <h3 style="text-align:center;">Form menambahkan Rumah </h3>
                        <div class="row">
                            <!-- <div class="col-md-6 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="Asset_Code"
                                        placeholder="Input Asset Code Here" style="width:100%;"> <label
                                        style="font-weight:bold; color:black;">Asset
                                        Code</label> </div>
                            </div> -->

                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="nama"
                                        placeholder="Input Asset Name Here" style="width:100%;" required> <label
                                        style="font-weight:bold; color:black;">Asset
                                        Name</label> </div>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="lokasi"
                                        placeholder="Input Location Here" style="width:100%;" required> <label
                                        style="font-weight:bold; color:black;">Location
                                    </label> </div>
                            </div>

                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="date" name="tgl_pengadaan"
                                        placeholder="Input Procurement Date Here" style="width:100%;" required> <label
                                        style="font-weight:bold; color:black;">Procurement
                                        Date </label> </div>
                            </div>
                            <div class="col-md-6 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Asset Type</label>
                                <select class="form-select" id="jenis" name="jenis">
                                    <option value="Tetap" selected>Tetap</option>
                                    <option value="Bergerak">Bergerak</option>
                                </select>
                            </div>

                            <div class="col-md-6 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Precondition</label>
                                <select class="form-select" id="kondisi" name="kondisi">
                                    <option value="Baik" selected>Baik</option>
                                    <option value="Rusak">Rusak</option>
                                </select>
                            </div>

                            <div class="col-md-4 mt-2 mb-2">
                                <div class="input-group input1"> <input type="number" name="kamar_tidur"
                                    placeholder="Input Number of Rooms Here" style="width:100%;" required> <label
                                    style="font-weight:bold; color:black;">Number of Rooms </label> </div>
                            </div>

                            
                            <div class="col-md-4 mt-2 mb-2">
                                <div class="input-group input1"> <input type="number" name="kamar_mandi"
                                    placeholder="Input Number of Bathrooms Here" style="width:100%;" required> <label
                                    style="font-weight:bold; color:black;">Number of Bathrooms </label> </div>
                            </div>

                            <div class="col-md-4 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Carport</label>
                                <select class="form-select" id="carport" name="carport">
                                    <option value="Ada" selected>Ada</option>
                                    <option value="Tidak_ada">Tidak ada</option>
                                </select>
                            </div>
                            
                            <div class="col-md-12 mt-2 mb-2">
                                <div class="input-group input1"> <input type="text" name="fasilitas"
                                        placeholder="Input Asset Name Here" style="width:100%;" required> <label
                                        style="font-weight:bold; color:black;">Fasilitas</label> </div>
                            </div>

                            <div class="col-md-12 mt-2 mb-2">
                                <label style="font-weight:bold; color:black;" class="mb-2">Upload Photos</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <input type="file" id="file-input" accept="image/png, image/jpeg" onchange="preview()" name="userfile[]" multiple>
                                <label for="file-input"><i class="bi bi-upload"></i>Choose Photo</label>
                            </div>
                            <div class="col-md-12 mt-2 mb-2">
                                <p for="num-of-files" id="num-of-files">No Files Chosen</p>
                                <div id="images"></div>
                            </div>

                        </div>
                        <br>
                        <div style="text-align:center;">
                            <button type="submit" class="btn btn-dark" onclick="coba()"> Submit </button>
                        </div>
                    </div>

                </form>
                <br>
            </div>

        </div>

        <script>
        let fileInput = document.getElementById("file-input");
        // console.log(fileInput);
        let imageContainer = document.getElementById("images");
        // console.log(imageContainer);
        let numOfFiles = document.getElementById("num-of-files");
        imageContainer.innerHTML = "";
        function preview(){
            numOfFiles.textContent = `${fileInput.files.length} Files Selected`;
            for(i of fileInput.files){
                let reader = new FileReader();
                let figure = document.createElement("figure");
                let figCap = document.createElement("figcaption");
                figCap.innerText = i.name;
                figure.appendChild(figCap);
                reader.onload=()=>{
                    let img = document.createElement("img");
                    img.setAttribute("src",reader.result);
                    figure.insertBefore(img,figCap);
                }
                imageContainer.appendChild(figure);
                reader.readAsDataURL(i);
            }


        }

        function coba(){
            var simpan=[];
            var name = document.getElementById('file-input'); 
            for (var i = 0; i < name.files.length; ++i) {
                simpan[i] = name.files.item(i);
                var inp = name.files.item(i);
                console.log("here is a file name: " + inp);
                }
            console.log(simpan);
            debugger;
        }
        </script>