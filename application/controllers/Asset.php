<?php 

class Asset extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Asset_model', 'asset');
        $this->load->library('form_validation');
    }

    //method yang digunakan untuk request data mahasiswa
    public function fetch_history_asset()
    {
        header('Content-Type: application/json');
        $kode = $this->input->post('kode', true);
        $list = $this->asset->get_datatables_history($kode);
        $data = array();
        $no = $this->input->post('start');
        //looping data mahasiswa
        foreach ($list as $Data_history) {
            $no++;
            $row = array();
            //row pertama akan kita gunakan untuk btn edit dan delete
            $row[] = $no;
            $row[] = $Data_history->tanggal_kegiatan;
            $row[] = $Data_history->kegiatan;
            $row[] = '<a class="btn btn-warning btn-sm" name="viewButton" data-id="' . $Data_history->id_history . '" data-toggle="modal" data-target="#history-modal" onclick="getDetailHistory(this)">detail</a>';
            
            $data[] = $row;
        }
        
        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->asset->get_count('history_asset'),
            "recordsFiltered" => $this->asset->count_filtered_history($kode),
            "data" => $data,
        );
        //output to json format
        $this->output->set_output(json_encode($output));
    }

    public function validation($name, $rules)
    {
        foreach($name as $row) 
        {
            $this->form_validation->set_rules($row, $row, $rules);
        }
    }

    public function dob_check($str){
        if (!DateTime::createFromFormat('Y-m-d', $str)) { //yes it's YYYY-MM-DD
            $this->form_validation->set_message('dob_check', 'The {field} has not a valid date format');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    private function set_upload_options()
    {   
        //upload an image options
        $config = array();
        $config['upload_path'] = './assets/img';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
        $config['max_size']      = '0';
        $config['overwrite']     = FALSE;

        return $config;
    }

    public function upload_file()
    {
        //upload file
        $this->load->library('upload');
        $dataInfo = array();
        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);
        for($i=0; $i<$cpt; $i++)
        {           
            $_FILES['userfile']['name']= $files['userfile']['name'][$i];
            $_FILES['userfile']['type']= $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error']= $files['userfile']['error'][$i];
            $_FILES['userfile']['size']= $files['userfile']['size'][$i];    

            // if (file_exists("./assets/img/".$_FILES['userfile']['name'])) {
            //     unlink("./assets/img/".$_FILES['userfile']['name']);
            // }

            $this->upload->initialize($this->set_upload_options());
            $this->upload->do_upload('userfile');
            $dataInfo[] = $this->upload->data();
        }

        return $dataInfo;
    }

    public function get_list_kode_asset()
    {
        $data_kode = [];
        $data = $this->asset->get_list_kode_asset()->result_array();

        foreach ($data as $list) {
            if ($list['status'] == 'available') {
                array_push($data_kode, $list['kode_asset']);
            }
        }

        echo json_encode($data_kode);

        return $data_kode;
    }

    public function get_info_delete()
    {
        $kode = $this->input->post('kode', true);
        $asset = $this->asset->get_deleted_asset($kode, 'asset')->row();

        if (isset($asset)) {
            $data = [
                'tanggal_delete' => $asset->tanggal_delete,
                'alasan_delete' => $asset->alasan_delete,
                'kode' => $kode
            ];

            echo json_encode($data);
            return $data;
        } else {
            $error = array(
                'error_code' => '404',
                'detail' => 'data not found',
                'kode' => $kode
            );
    
            echo json_encode($error);
            return $error;
        }
    }

    public function get_history_json()
    {
        $kode = $this->input->post('kode');
        $data = json_encode($this->asset->get_json_history($kode));
        echo $data;
        return $data;
    }

    public function get_detail_history_json()
    {
        $id_history = $this->input->post('id_history');
        $data = json_encode($this->asset->get_json_detail_history($id_history));
        echo $data;
        return $data;
    }

    public function form_validation_history()
    {
        $inputString = ['kronologi', 'kondisi', 'action_plan', 'RAB'];
        $inputDate = ['tgl_kejadian'];
        $this->validation($inputString, 'trim|required');
        $this->validation($inputDate, 'trim|required|callback_dob_check');

        if ($this->form_validation->run() == TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}