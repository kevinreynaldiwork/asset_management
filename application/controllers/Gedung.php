<?php 

require_once(APPPATH."controllers/Asset.php");

class Gedung extends Asset
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Gedung_model', 'gedung');
        $this->load->helper('date');
        $this->load->library('session');
    }
    
    public function index(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'admin') {
            $this->load->view('templates/header');
            $this->load->view('gedung/index');
            $this->load->view('templates/footer');
        } else {
            redirect('login/index');
        }
    }

    public function index_users(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'user') {
            $this->load->view('templates/header_users');
            $this->load->view('gedung/index_users');
            $this->load->view('templates/footer_users');
        } else {
            redirect('login/index');
        }
    }

    public function add_gedung_form(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'admin') {
            $this->load->view('templates/header');
            $this->load->view('gedung/add_gedung');
            $this->load->view('templates/footer');
        } else {
            redirect('login/index');
        }
    }

    public function perbaikan_asset(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'admin') {
            $this->load->view('templates/header');
            $this->load->view('gedung/perbaikan_asset');
            $this->load->view('templates/footer');
        } else {
            redirect('login/index');
        }
    }

    public function edit_gedung_form(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'admin') {
            $this->load->view('templates/header');
            $this->load->view('gedung/edit_gedung');
            $this->load->view('templates/footer');
        } else {
            redirect('login/index');
        }
    }

    public function get_gedung_json()
    {
        $kode = $this->input->post('kode');
        $data = json_encode($this->gedung->get_json_gedung($kode));
        echo $data;
        return $data;
    }

    //method yang digunakan untuk request data mahasiswa
    public function fetch_gedung_user()
    {
        header('Content-Type: application/json');
        $list = $this->gedung->get_datatables_user();
        $data = array();
        $no = $this->input->post('start');
        //looping data mahasiswa
        foreach ($list as $Data_gedung) {
            $no++;
            $row = array();
            //row pertama akan kita gunakan untuk btn edit dan delete
            $row[] = $Data_gedung->kode_asset;
            $row[] = $Data_gedung->nama;
            
            $gedung = $this->asset->get_asset($Data_gedung->kode_asset, 'detail_asset')->row();
            $json = json_decode($gedung->info_asset,true);
            $row[] = $json["peruntukan"];

            $row[] = $Data_gedung->lokasi;
            $row[] = $Data_gedung->tanggal_terima;
            
            if($Data_gedung->status=='available'){
                $row[] = "<span style='background-color:#7AFFB0;'>".$Data_gedung->status."</span>";
            }
            else if ($Data_gedung->status=='deleted'){
                $row[] = "<span style='background-color:#FF0000;'>".$Data_gedung->status."</span>";
            } else {
                $row[] = "<span style='background-color:#FFFF00;'>".$Data_gedung->status."</span>";
            }

            $row[] = '<a type="button" class="btn btn-primary" name="detail-modal" data-toggle="modal" data-id="'.$Data_gedung->kode_asset.'" data-target="#detail-modal" onclick="getDetail(this)"><i class="bi bi-file-earmark-text"></i></a>';
            $data[] = $row;
        }
        
        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->gedung->count_all(),
            "recordsFiltered" => $this->gedung->count_filtered_user(),
            "data" => $data,
        );
        //output to json format
        $this->output->set_output(json_encode($output));
    }

    //method yang digunakan untuk request data mahasiswa
    public function fetch_gedung_admin()
    {
        header('Content-Type: application/json');
        $list = $this->gedung->get_datatables();
        $data = array();
        $no = $this->input->post('start');
        //looping data mahasiswa
        foreach ($list as $Data_gedung) {
            $no++;
            $row = array();
            //get peruntukan dan gedung value
            $gedung = $this->asset->get_asset($Data_gedung->kode_asset, 'detail_asset')->row();
            $json = json_decode($gedung->info_asset,true);
            $row[] = $json["gedung"];
            $row[] = $Data_gedung->kode_asset;
            $row[] = $Data_gedung->nama;
            $row[] = $json["peruntukan"];
            $row[] = $Data_gedung->lokasi;
            $row[] = $Data_gedung->tanggal_terima;
            
            if($Data_gedung->status=='available'){
                $row[] = "<span style='background-color:#7AFFB0;'>".$Data_gedung->status."</span>";
            }
            else if ($Data_gedung->status=='deleted'){
                $row[] = "<span style='background-color:#FF0000;'>".$Data_gedung->status."</span>";
            } else {
                $row[] = "<span style='background-color:#FFFF00;'>".$Data_gedung->status."</span>";
            }

            $row[] =  '<a class="btn btn-success btn-sm" name="editButton" data-id="' . $Data_gedung->kode_asset . '" data-bs-toggle="modal" data-bs-target="#edit_gedung" onclick="editGedung(this)"><i class="bi bi-pencil-square"></i></a>
            <a class="btn btn-danger btn-sm " name="deleteButton" data-id="' . $Data_gedung->kode_asset . '" data-bs-toggle="modal" data-bs-target="#delete_asset" onclick="deleteClick(this)"><i class="bi bi-trash"></i></a>
            <a class="btn btn-warning btn-sm " name="addPerbaikanButton" data-id="' . $Data_gedung->kode_asset . '" data-bs-toggle="modal"  data-bs-target="#perbaikan_asset" onclick="perbaikanAsset(this)" ><i class="bi bi-hammer"></i></a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->gedung->count_all(),
            "recordsFiltered" => $this->gedung->count_filtered(),
            "data" => $data,
        );
        //output to json format
        $this->output->set_output(json_encode($output));
    }

    private function form_validation()
    {
        $inputString = ['nama', 'lokasi', 'jenis', 'kondisi', 'carport', 'fasilitas', 'peruntukan', 'gedung'];
        $inputNumber = ['kamar_tidur', 'kamar_mandi'];
        $inputDate = ['tgl_pengadaan'];
        $this->validation($inputString, 'trim|required');
        $this->validation($inputNumber, 'trim|required|numeric');
        $this->validation($inputDate, 'trim|required|callback_dob_check');

        if ($this->form_validation->run() == TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function add_gedung() 
    {
        $image = "";
        $data = [];
        $dataInfo = [];
        $data['judul'] = 'Add Gedung';

        //form validation
        $validation = $this->form_validation();
        
        //upload file
        $dataInfo = $this->upload_file();

        $nomor = $this->asset->get_asset_count('gedung') + 1;
        $tgl_pengadaan = $this->input->post('tgl_pengadaan', true);
        $kode = strtoupper($tgl_pengadaan . '/' . substr($this->input->post('nama'), 0, 1) . '/' . 'UBS' . '/' . $nomor);

        if ($validation == TRUE) {
            $result = $this->gedung->add_gedung($dataInfo, $kode);
            echo json_encode($result);
            // $this->session->set_flashdata('flash', 'Ditambahkan');
            // redirect('peoples');
            redirect(base_url('/gedung/index'));
        } else {
            echo json_encode(array(
                "status"=> 400,
                "message"=> validation_errors(),
            ));
        }
    }

    public function delete_gedung()
    {
        $data = [];
        $data['judul'] = 'Delete Gedung';
        $this->form_validation->set_rules('kode', 'kode', 'trim|required');

        if ($this->form_validation->run() == TRUE) {
            $kode = $this->input->post('kode', true);
            if ($this->asset->get_asset_count_by_kode($kode) > 0) {
                $result = $this->asset->delete_asset($kode, date("Y-m-d",time()));
                if ($result == 1) {
                    $gedung = $this->gedung->get_json_gedung($kode);
                    echo json_encode($gedung);
                    redirect(base_url('/gedung/index'));
                } else {
                    echo json_encode($result);
                    redirect(base_url('/gedung/index'));
                }
            } else {
                $error = array(
                    'error_code' => '404',
                    'detail' => 'data not found or data has been deleted',
                    'kode' => $kode
                );
    
                echo json_encode($error);
            }
        } else {
            $error = array(
                'error_code' => '400',
                'detail' => 'no kode_asset is given',
            );

            echo json_encode($error);
        }
        //redirect('mahasiswa');
    }

    public function edit_gedung()
    {
        $data = [];
        $data['judul'] = 'Edit Gedung';

        //form validation
        $validation = $this->form_validation();
        $this->form_validation->set_rules('kode', 'kode', 'trim|required');

        //upload file
        $dataInfo = $this->upload_file();

        if ($this->form_validation->run() == TRUE && $validation == TRUE) {
            if ($dataInfo[0]["file_name"] == "") {
                $result = $this->gedung->edit_gedung("");
            } else {
                $result = $this->gedung->edit_gedung($dataInfo);
            }
            echo json_encode($result);
            redirect(base_url('/gedung/index'));
        } else {
            echo json_encode(array(
                "status"=> 400,
                "message"=> "request body tidak sesuai dengan form validation",
            ));
        }
    }

    public function add_perbaikan() 
    {
        $data = []; 
        $data['judul'] = 'Add Perbaikan Asset';
        $kode = $this->input->post('kode', true);

        //form validation
        $validation = $this->form_validation_history();

        //upload file
        $dataInfo = $this->upload_file();

        if ($validation == true) {
            $result = $this->asset->add_history($kode, $dataInfo);
            echo json_encode($result);
            redirect(base_url('/gedung/index'));
        } else {
            echo json_encode(array(
                "status"=> 400,
                "message"=> validation_errors(),
            ));
        }
    }
}