<?php 

require_once(APPPATH."controllers/Asset.php");

class Kendaraan extends Asset
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('kendaraan_model', 'kendaraan');
        $this->load->helper('date');
        $this->load->library('session');
    }

    public function index(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'admin') {
            $this->load->view('templates/header');
            $this->load->view('kendaraan/index');
            $this->load->view('templates/footer');
        } else {
            redirect('login/index');
        }
    }

    public function index_users(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'user') {
            $this->load->view('templates/header_users');
            $this->load->view('kendaraan/index_users');
            $this->load->view('templates/footer_users');
        } else {
            redirect('login/index');
        }
    }

    public function add_kendaraan_form(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'admin') {
            $this->load->view('templates/header');
            $this->load->view('kendaraan/add_kendaraan');
            $this->load->view('templates/footer');
        } else {
            redirect('login/index');
        }
    }

    public function perbaikan_asset(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'admin') {
            $this->load->view('templates/header');
            $this->load->view('kendaraan/perbaikan_asset');
            $this->load->view('templates/footer');
        } else {
            redirect('login/index');
        }
    }

    public function edit_kendaraan_form(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'admin') {
            $this->load->view('templates/header');
            $this->load->view('kendaraan/edit_kendaraan');
            $this->load->view('templates/footer');
        } else {
            redirect('login/index');
        }
    }

    //method yang digunakan untuk request data mahasiswa
    public function fetch_kendaraan_user()
    {
        header('Content-Type: application/json');
        $list = $this->kendaraan->get_datatables_user();
        $data = array();
        $no = $this->input->post('start');
        //looping data mahasiswa
        foreach ($list as $Data_kendaraan) {
            $no++;
            $row = array();
            //row pertama akan kita gunakan untuk btn edit dan delete
            $row[] = $Data_kendaraan->kode_asset;
            
            $asset = $this->asset->get_asset($Data_kendaraan->kode_asset, 'asset')->row();
            $row[] = $asset->nama;
            
            $kendaraan = $this->asset->get_asset($Data_kendaraan->kode_asset, 'detail_asset')->row();
            $json = json_decode($kendaraan->info_asset,true);
            $row[] = $json["nopol"];
            
            $row[] = $Data_kendaraan->kategori;
            $row[] = $Data_kendaraan->tanggal_terima;

            //$user = $this->asset->get_user_asset($Data_kendaraan->id_user);
            $user = $this->asset->get_user_by_kode($Data_kendaraan->kode_asset)->row();
            if ($user) {
                $row[] = $user->nama;
            } else {
                $row[] = null;
            }
            $row[] = $Data_kendaraan->departemen;
            
            if($Data_kendaraan->status=='available'){
                $row[] = "<span style='background-color:#7AFFB0;'>".$Data_kendaraan->status."</span>";
            }
            else if ($Data_kendaraan->status=='deleted'){
                $row[] = "<span style='background-color:#FF0000;'>".$Data_kendaraan->status."</span>";
            } else {
                $row[] = "<span style='background-color:#FFFF00;'>".$Data_kendaraan->status."</span>";
            }

            $row[] = '<a type="button" class="btn btn-primary" name="detail-modal" data-toggle="modal" data-id="'.$Data_kendaraan->kode_asset.'" data-target="#detail-modal" onclick="getDetail(this)"><i class="bi bi-file-earmark-text"></i></a>';
            $data[] = $row;
        }
        
        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->kendaraan->count_all(),
            "recordsFiltered" => $this->kendaraan->count_filtered_user(),
            "data" => $data,
        );
        //output to json format
        $this->output->set_output(json_encode($output));
    }

    public function get_kendaraan_json()
    {
        $kode = $this->input->post('kode');
        $data = json_encode($this->kendaraan->get_json_kendaraan($kode));
        echo $data;
        return $data;
    }
    
    //method yang digunakan untuk request data mahasiswa
    public function fetch_kendaraan_admin()
    {
        header('Content-Type: application/json');
        $list = $this->kendaraan->get_datatables();
        $data = array();
        $no = $this->input->post('start');
        //looping data mahasiswa
        foreach ($list as $Data_kendaraan) {
            $no++;
            $row = array();
            //get peruntukan dan gedung value
            $kendaraan = $this->asset->get_asset($Data_kendaraan->kode_asset, 'detail_asset')->row();
            $json = json_decode($kendaraan->info_asset,true);
            $row[] = $Data_kendaraan->kode_asset; 
            $row[] = $Data_kendaraan->nama;
            $row[] = $json["nopol"];
            $row[] = $Data_kendaraan->kategori;
            $row[] = $Data_kendaraan->tanggal_terima;

            if($Data_kendaraan->status=='available'){
                $row[] = "<span style='background-color:#7AFFB0;'>".$Data_kendaraan->status."</span>";
            }
            else if ($Data_kendaraan->status=='deleted'){
                $row[] = "<span style='background-color:#FF0000;'>".$Data_kendaraan->status."</span>";
            } else {
                $row[] = "<span style='background-color:#FFFF00;'>".$Data_kendaraan->status."</span>";
            }

            $row[] =  '<a class="btn btn-success btn-sm" name="editButton" data-id="' . $Data_kendaraan->kode_asset . '" data-bs-toggle="modal" data-bs-target="#edit_kendaraan" onclick="editKendaraan(this)"><i class="bi bi-pencil-square"></i></a>
            <a class="btn btn-danger btn-sm " name="deleteButton" data-id="' . $Data_kendaraan->kode_asset . '" data-bs-toggle="modal" data-bs-target="#delete_asset" onclick="deleteClick(this)"><i class="bi bi-trash"></i></a>
            <a class="btn btn-warning btn-sm " name="addPerbaikanButton" data-id="' . $Data_kendaraan->kode_asset . '" data-bs-toggle="modal"  data-bs-target="#perbaikan_asset" onclick="perbaikanAsset(this)" ><i class="bi bi-hammer"></i></a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->kendaraan->count_all(),
            "recordsFiltered" => $this->kendaraan->count_filtered(),
            "data" => $data,
        );
        //output to json format
        $this->output->set_output(json_encode($output));
    }

    private function form_validation()
    {
        $inputString = ['nama', 'lokasi', 'jenis', 'kondisi', 'kategori', 'nopol', 'nomor_mesin', 'pic'];
        $inputDate = ['tgl_pengadaan', 'durasi_pajak', 'durasi_plat'];
        $this->validation($inputString, 'trim|required');
        $this->validation($inputDate, 'trim|required|callback_dob_check');

        if ($this->form_validation->run() == TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function add_kendaraan() 
    {
        $kategori = ['mobil', 'motor'];
        $image = "";
        $data = [];
        $dataInfo = [];
        $data['judul'] = 'Add kendaraan';

        //form validation
        $validation = $this->form_validation();
        
        //upload file
        $dataInfo = $this->upload_file();

        $nomor = $this->asset->get_asset_count($kategori) + 1;
        $tgl_pengadaan = $this->input->post('tgl_pengadaan', true);
        $kode = strtoupper($tgl_pengadaan . '/' . 'K' . '/' . 'UBS' . '/' . $nomor);

        if ($validation == TRUE) {
            $result = $this->kendaraan->add_kendaraan($dataInfo, $kode);
            echo json_encode($result);
            redirect(base_url('/kendaraan/index'));
            // $this->session->set_flashdata('flash', 'Ditambahkan');
            // redirect('peoples');
        } else {
            echo json_encode(array(
                "status"=> 400,
                "message"=> validation_errors(),
            ));
        }
    }

    public function delete_kendaraan()
    {
        $data = [];
        $data['judul'] = 'Delete kendaraan';
        $this->form_validation->set_rules('kode', 'kode', 'trim|required');

        if ($this->form_validation->run() == TRUE) {
            $kode = $this->input->post('kode', true);
            if ($this->asset->get_asset_count_by_kode($kode) > 0) {
                $result = $this->asset->delete_asset($kode, date("Y-m-d",time()));
                if ($result == 1) {
                    $kendaraan = $this->kendaraan->get_json_kendaraan($kode);
                    echo json_encode($kendaraan);
                    redirect(base_url('/kendaraan/index'));
                } else {
                    echo json_encode($result);
                    redirect(base_url('/kendaraan/index'));
                }
            } else {
                $error = array(
                    'error_code' => '404',
                    'detail' => 'data not found or data has been deleted',
                    'kode' => $kode
                );
    
                echo json_encode($error);
            }
        } else {
            $error = array(
                'error_code' => '400',
                'detail' => 'no kode_asset is given',
            );

            echo json_encode($error);
        }
        //redirect('mahasiswa');
    }

    public function edit_kendaraan()
    {
        $data = [];
        $data['judul'] = 'Edit kendaraan';

        //form validation
        $validation = $this->form_validation();
        $this->form_validation->set_rules('kode', 'kode', 'trim|required');

        //upload file
        $dataInfo = $this->upload_file();

        if ($validation == TRUE && $this->form_validation->run() == TRUE) {
            if ($dataInfo[0]["file_name"] == "") {
                $result = $this->kendaraan->edit_kendaraan("");
            } else {
                $result = $this->kendaraan->edit_kendaraan($dataInfo);
            }
            echo json_encode($result);
            redirect(base_url('/kendaraan/index'));
        } else {
            echo json_encode(array(
                "status"=> 400,
                "message"=> "request body tidak sesuai dengan form validation",
            ));
        }
    }

    public function add_perbaikan() 
    {
        $data = []; 
        $data['judul'] = 'Add Perbaikan Asset';
        $kode = $this->input->post('kode', true);

        //form validation
        $validation = $this->form_validation_history();

        //upload file
        $dataInfo = $this->upload_file();

        if ($validation == true) {
            $result = $this->asset->add_history($kode, $dataInfo);
            echo json_encode($result);
            redirect(base_url('/kendaraan/index'));
        } else {
            echo json_encode(array(
                "status"=> 400,
                "message"=> validation_errors(),
            ));
        }
    }
}



