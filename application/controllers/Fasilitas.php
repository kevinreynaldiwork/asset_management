<?php

require_once(APPPATH."controllers/Asset.php");

class Fasilitas extends Asset
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Fasilitas_model', 'fasilitas');
        $this->load->helper('date');
    }

    public function index(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'admin') {
            $this->load->view('templates/header');
            $this->load->view('fasilitas/index');
            $this->load->view('templates/footer');
        } else {
            redirect('login/index');
        }
    }

    public function index_users(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'user') {
            $this->load->view('templates/header_users');
            $this->load->view('fasilitas/index_users');
            $this->load->view('templates/footer_users');
        } else {
            redirect('login/index');
        }
    }

    public function add_fasilitas_form(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'admin') {
            $this->load->view('templates/header');
            $this->load->view('fasilitas/add_fasilitas');
            $this->load->view('templates/footer');
        } else {
            redirect('login/index');
        }
    }
    public function perbaikan_asset(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'admin') {
            $this->load->view('templates/header');
            $this->load->view('fasilitas/perbaikan_asset');
            $this->load->view('templates/footer');
        } else {
            redirect('login/index');
        }
    }

    public function edit_fasilitas_form(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'admin') {
            $this->load->view('templates/header');
            $this->load->view('fasilitas/edit_fasilitas');
            $this->load->view('templates/footer');    
        } else {
            redirect('login/index');
        }
    }

    public function get_fasilitas_json()
    {
        $kode = $this->input->post('kode');
        $data = json_encode($this->fasilitas->get_json_fasilitas($kode));
        echo $data;
        return $data;
    }

    //method yang digunakan untuk request data mahasiswa
    public function fetch_fasilitas_user()
    {
        header('Content-Type: application/json');
        $list = $this->fasilitas->get_datatables_user();
        $data = array();
        $no = $this->input->post('start');
        //looping data mahasiswa
        foreach ($list as $Data_fasilitas) {
            $no++;
            $row = array();
            $row[] = $Data_fasilitas->kode_asset;
            $asset = $this->asset->get_asset($Data_fasilitas->kode_asset, 'asset')->row();
            $row[] = $asset->nama;
            $row[] = $Data_fasilitas->lokasi;
            $row[] = $Data_fasilitas->tanggal_terima;
            
            if($Data_fasilitas->status=='available'){
                $row[] = "<span style='background-color:#7AFFB0;'>".$Data_fasilitas->status."</span>";
            }
            else if ($Data_fasilitas->status=='deleted'){
                $row[] = "<span style='background-color:#FF0000;'>".$Data_fasilitas->status."</span>";
            } else {
                $row[] = "<span style='background-color:#FFFF00;'>".$Data_fasilitas->status."</span>";
            }
           
            $user = $this->asset->get_user_by_kode($Data_fasilitas->kode_asset)->row();
            if ($user) {
                $row[] = $user->nama;
            } else {
                $row[] = null;
            }
            $row[] = $Data_fasilitas->departemen;
            $row[] = '<a type="button" class="btn btn-primary" name="detail-modal" data-toggle="modal" data-id="'.$Data_fasilitas->kode_asset.'" data-target="#detail-modal" onclick="getDetail(this)"><i class="bi bi-file-earmark-text"></i></a>';

            $data[] = $row;
        }
        
        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->fasilitas->count_all(),
            "recordsFiltered" => $this->fasilitas->count_filtered_user(),
            "data" => $data,
        );
        //output to json format
        $this->output->set_output(json_encode($output));
    }

    //method yang digunakan untuk request data mahasiswa
    public function fetch_fasilitas_admin()
    {
        header('Content-Type: application/json');
        $list = $this->fasilitas->get_datatables();
        $data = array();
        $no = $this->input->post('start');
        //looping data mahasiswa
        foreach ($list as $Data_fasilitas) {
            $no++;
            $row = array();
            //row pertama akan kita gunakan untuk btn edit dan delete
            $row[] = $Data_fasilitas->kode_asset;
            $row[] = $Data_fasilitas->nama;
            $row[] = $Data_fasilitas->lokasi;
            $row[] = $Data_fasilitas->tanggal_terima;

            if($Data_fasilitas->status=='available'){
                $row[] = "<span style='background-color:#7AFFB0;'>".$Data_fasilitas->status."</span>";
            }
            else if ($Data_fasilitas->status=='deleted'){
                $row[] = "<span style='background-color:#FF0000;'>".$Data_fasilitas->status."</span>";
            } else {
                $row[] = "<span style='background-color:#FFFF00;'>".$Data_fasilitas->status."</span>";
            }

            $row[] =  '<a class="btn btn-success btn-sm" name="editButton" data-id="' . $Data_fasilitas->kode_asset . '" data-bs-toggle="modal" data-bs-target="#edit_fasilitas" onclick="editFasilitas(this)"><i class="bi bi-pencil-square"></i></a>
            <a class="btn btn-danger btn-sm " name="deleteButton" data-id="' . $Data_fasilitas->kode_asset . '" data-bs-toggle="modal" data-bs-target="#delete_asset" onclick="deleteClick(this)"><i class="bi bi-trash"></i></a>
            <a class="btn btn-warning btn-sm " name="addPerbaikanButton" data-id="' . $Data_fasilitas->kode_asset . '" data-bs-toggle="modal"  data-bs-target="#perbaikan_asset" onclick="perbaikanAsset(this)"><i class="bi bi-hammer"></i></a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->fasilitas->count_all(),
            "recordsFiltered" => $this->fasilitas->count_filtered(),
            "data" => $data,
        );
        //output to json format
        $this->output->set_output(json_encode($output));
    }

    private function form_validation()
    {
        $inputString = ['nama', 'lokasi', 'jenis', 'kondisi', 'garansi'];
        $inputDate = ['tgl_pengadaan'];
        $this->validation($inputString, 'trim|required');
        $this->validation($inputDate, 'trim|required|callback_dob_check');

        if ($this->form_validation->run() == TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function add_fasilitas() 
    {
        $image = "";
        $data = [];
        $dataInfo = [];
        $data['judul'] = 'Add Fasilitas';

        //form validation
        $validation = $this->form_validation();
        
        //upload file
        $dataInfo = $this->upload_file();

        $nomor = $this->asset->get_asset_count('fasilitas') + 1;
        $tgl_pengadaan = $this->input->post('tgl_pengadaan', true);
        $kode = strtoupper($tgl_pengadaan . '/' . 'F' . '/' . 'UBS' . '/' . $nomor);

        if ($validation == TRUE) {
            $result = $this->fasilitas->add_fasilitas($dataInfo, $kode);
            echo json_encode($result);
            redirect(base_url('/fasilitas/index'));
            //redirect(base_url('/fasilitas/index'));
            // $this->session->set_flashdata('flash', 'Ditambahkan');
            // redirect('peoples');
        } else {
            echo json_encode(array(
                "status"=> 400,
                "message"=> validation_errors(),
            ));
        }
    }

    public function delete_fasilitas()
    {
        $data = [];
        $data['judul'] = 'Delete Fasilitas';
        $this->form_validation->set_rules('kode', 'kode', 'trim|required');

        if ($this->form_validation->run() == TRUE) {
            $kode = $this->input->post('kode', true);
            if ($this->asset->get_asset_count_by_kode($kode) > 0) {
                $result = $this->asset->delete_asset($kode, date("Y-m-d",time()));
                if ($result == 1) {
                    $fasilitas = $this->fasilitas->get_json_fasilitas($kode);
                    echo json_encode($fasilitas);
                    redirect(base_url('/fasilitas/index'));
                } else {
                    echo json_encode($result);
                    redirect(base_url('/fasilitas/index'));
                }
            } else {
                $error = array(
                    'error_code' => '404',
                    'detail' => 'data not found or data has been deleted',
                    'kode' => $kode
                );
    
                echo json_encode($error);
            }
        } else {
            $error = array(
                'error_code' => '400',
                'detail' => 'no kode_asset is given',
            );

            echo json_encode($error);
        }
        //redirect('mahasiswa');
    }

    public function edit_fasilitas()
    {
        $data = [];
        $data['judul'] = 'Edit Fasilitas';

        //form validation
        $validation = $this->form_validation();
        $this->form_validation->set_rules('kode', 'kode', 'trim|required');

        //upload file
        $dataInfo = $this->upload_file();

        if ($validation == TRUE && $this->form_validation->run() == TRUE) {
            if ($dataInfo[0]["file_name"] == "") {
                $result = $this->fasilitas->edit_fasilitas("");
            } else {
                $result = $this->fasilitas->edit_fasilitas($dataInfo);
            }
            echo json_encode($result);
            redirect(base_url('/fasilitas/index'));
        } else {
            echo json_encode(array(
                "status"=> 400,
                "message"=> "request body tidak sesuai dengan form validation",
            ));
        }
    }

    public function add_perbaikan() 
    {
        $data = []; 
        $data['judul'] = 'Add Perbaikan Asset';
        $kode = $this->input->post('kode', true);

        //form validation
        $validation = $this->form_validation_history();

        //upload file
        $dataInfo = $this->upload_file();

        if ($validation == true) {
            $result = $this->asset->add_history($kode, $dataInfo);
            echo json_encode($result);
            redirect(base_url('/fasilitas/index'));
        } else {
            echo json_encode(array(
                "status"=> 400,
                "message"=> validation_errors(),
            ));
        }
    }
}