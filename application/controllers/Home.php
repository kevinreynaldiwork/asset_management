<?php

class Home extends CI_Controller{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    public function index(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'admin') {
            $data['CI_Version'] = CI_VERSION;
            $this->load->view('templates/header');
            $this->load->view('home/index', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('login/index');
        }
    }

    public function user(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'user') {
            $this->load->view('templates/header_users');
            $this->load->view('home/index_users');
            $this->load->view('templates/footer_users');
        } else {
            redirect('login/index');
        }
    }

}