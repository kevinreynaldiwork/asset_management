<?php 

require_once(APPPATH."controllers/Asset.php");

class Asrama extends Asset
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Asrama_model', 'asrama');
        $this->load->helper('date');
    }
    public function index(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'admin') {
            $this->load->view('templates/header');
            $this->load->view('asrama/index');
            $this->load->view('templates/footer');
        } else {
            redirect('login/index');
        }
    }

    public function index_users(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'user') {
            $this->load->view('templates/header_users');
            $this->load->view('asrama/index_users');
            $this->load->view('templates/footer_users');
        } else {
            redirect('login/index');
        }
    }

    public function add_asrama_form(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'admin') {
            $this->load->view('templates/header');
            $this->load->view('asrama/add_asrama');
            $this->load->view('templates/footer');
        } else {
            redirect('login/index');
        }
    }
    public function perbaikan_asset(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'admin') {
            $this->load->view('templates/header');
            $this->load->view('asrama/perbaikan_asset');
            $this->load->view('templates/footer');
        } else {
            redirect('login/index');
        }
    }

    public function get_asrama_json()
    {
        $kode = $this->input->post('kode');
        $data = json_encode($this->asrama->get_json_asrama($kode));
        echo $data;
        return $data;
    }

    //method yang digunakan untuk request data mahasiswa
    public function fetch_asrama_user()
    {
        header('Content-Type: application/json');
        $list = $this->asrama->get_datatables_user();
        $data = array();
        $no = $this->input->post('start');
        //looping data mahasiswa
        foreach ($list as $Data_asrama) {
            $no++;
            $row = array();
            //row pertama akan kita gunakan untuk btn edit dan delete
            $row[] = $Data_asrama->kode_asset;
            $row[] = $Data_asrama->nama;
            
            $asrama = $this->asset->get_asset($Data_asrama->kode_asset, 'detail_asset')->row();
            $json = json_decode($asrama->info_asset,true);
            $row[] = $json["lantai"];
            $row[] = $json["kamar"];
            $row[] = $json["kapasitas"];
            $row[] = $json["penghuni"];
           
            if($Data_asrama->status=='available'){
                $row[] = "<span style='background-color:#7AFFB0;'>".$Data_asrama->status."</span>";
            }
            else if ($Data_asrama->status=='deleted'){
                $row[] = "<span style='background-color:#FF0000;'>".$Data_asrama->status."</span>";
            } else {
                $row[] = "<span style='background-color:#FFFF00;'>".$Data_asrama->status."</span>";
            }

            $row[] = '<a type="button" class="btn btn-primary" name="detail-modal" data-toggle="modal" data-id="'.$Data_asrama->kode_asset.'" data-target="#detail-modal" onclick="getDetail(this)"><i class="bi bi-file-earmark-text"></i></a>';
            $data[] = $row;
        }
        
        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->asrama->count_all(),
            "recordsFiltered" => $this->asrama->count_filtered_user(),
            "data" => $data,
        );
        //output to json format
        $this->output->set_output(json_encode($output));
    }

    //method yang digunakan untuk request data mahasiswa
    public function fetch_asrama_admin()
    {
        header('Content-Type: application/json');
        $list = $this->asrama->get_datatables();
        $data = array();
        $no = $this->input->post('start');
        //looping data mahasiswa
        foreach ($list as $Data_asrama) {
            $no++;
            $row = array();
            //get peruntukan dan gedung value
            $row[] = $Data_asrama->kode_asset;
            $row[] = $Data_asrama->nama;
            $asrama = $this->asset->get_asset($Data_asrama->kode_asset, 'detail_asset')->row();
            $json = json_decode($asrama->info_asset,true);
            $row[] = $json["lantai"];
            $row[] = $json["kamar"];
            $row[] = $json["kapasitas"];
            $row[] = $json["penghuni"];

            if($Data_asrama->status=='available'){
                $row[] = "<span style='background-color:#7AFFB0;'>".$Data_asrama->status."</span>";
            }
            else if ($Data_asrama->status=='deleted'){
                $row[] = "<span style='background-color:#FF0000;'>".$Data_asrama->status."</span>";
            } else {
                $row[] = "<span style='background-color:#FFFF00;'>".$Data_asrama->status."</span>";
            }

            $row[] =  '<a class="btn btn-success btn-sm" name="editButton" data-id="' . $Data_asrama->kode_asset . '" data-bs-toggle="modal" data-bs-target="#edit_asrama" onclick="editAsrama(this)"><i class="bi bi-pencil-square"></i></a>
            <a class="btn btn-danger btn-sm " name="deleteButton" data-id="' . $Data_asrama->kode_asset . '" data-bs-toggle="modal" data-bs-target="#delete_asset" onclick="deleteClick(this)"><i class="bi bi-trash"></i></a>
            <a class="btn btn-warning btn-sm " name="addPerbaikanButton" data-id="' . $Data_asrama->kode_asset . '" data-bs-toggle="modal"  data-bs-target="#perbaikan_asset" onclick="perbaikanAsset(this)"><i class="bi bi-hammer"></i></a>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->asrama->count_all(),
            "recordsFiltered" => $this->asrama->count_filtered(),
            "data" => $data,
        );
        //output to json format
        $this->output->set_output(json_encode($output));
    }

    private function form_validation()
    {
        $inputString = ['asrama'];
        $inputNumber = ['lantai', 'kamar', 'penghuni', 'kapasitas'];
        $inputDate = ['tgl_pengadaan'];
        $this->validation($inputString, 'trim|required');
        $this->validation($inputNumber, 'trim|required|numeric');
        $this->validation($inputDate, 'trim|required|callback_dob_check');

        if ($this->form_validation->run() == TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function add_asrama() 
    {
        $image = "";
        $data = [];
        $dataInfo = [];
        $data['judul'] = 'Add Asrama';

        //form validation
        $validation = $this->form_validation();
        
        //upload file
        $dataInfo = $this->upload_file();

        $target = $this->input->post('fasilitas');
        $arr = explode(",", implode(",",$target));

        $jumlah_target = $this->input->post('jumlah_fasilitas');
        $arr_target = explode(",", implode(",",$jumlah_target));

        $nomor = $this->asset->get_asset_count('asrama') + 1;
        $tgl_pengadaan = $this->input->post('tgl_pengadaan', true);
        $kode = strtoupper($tgl_pengadaan . '/' . 'A' . '/' . 'UBS' . '/' . $nomor);

        if ($validation == TRUE) {
            $result = $this->asrama->add_asrama($dataInfo, $kode, $arr, $arr_target);
            echo json_encode($result);
            // $this->session->set_flashdata('flash', 'Ditambahkan');
            // redirect('peoples');
            redirect(base_url('/asrama/index'));
            
        } else {
            echo json_encode(array(
                "status"=> 400,
                "message"=> validation_errors(),
            ));
        }
    }

    public function delete_asrama()
    {
        $data = [];
        $data['judul'] = 'Delete Asrama';
        $this->form_validation->set_rules('kode', 'kode', 'trim|required');

        if ($this->form_validation->run() == TRUE) {
            $kode = $this->input->post('kode', true);
            if ($this->asset->get_asset_count_by_kode($kode) > 0) {
                $result = $this->asset->delete_asset($kode, date("Y-m-d",time()));
                if ($result == 1) {
                    $asrama = $this->asrama->get_json_asrama($kode);
                    echo json_encode($asrama);
                    redirect(base_url('/asrama/index'));
                } else {
                    echo json_encode($result);
                    redirect(base_url('/asrama/index'));
                }
            } else {
                $error = array(
                    'error_code' => '404',
                    'detail' => 'data not found or data has been deleted',
                    'kode' => $kode
                );
    
                echo json_encode($error);
            }
        } else {
            $error = array(
                'error_code' => '400',
                'detail' => 'no kode_asset is given',
            );

            echo json_encode($error);
        }
        //redirect('mahasiswa');
    }

    public function edit_asrama()
    {
        $data = [];
        $data['judul'] = 'Edit Asrama';

        //form validation
        $validation = $this->form_validation();
        $this->form_validation->set_rules('kode', 'kode', 'trim|required');

        $target = $this->input->post('fasilitas');
        $arr = explode(",", implode(",",$target));

        $jumlah_target = $this->input->post('jumlah_fasilitas');
        $arr_target = explode(",", implode(",",$jumlah_target));

        //upload file
        $dataInfo = $this->upload_file();
        
        if ($validation == TRUE && $this->form_validation->run() == TRUE) {
            if ($dataInfo[0]["file_name"] == "") {
                $result = $this->asrama->edit_asrama("", $arr, $arr_target);
            } else {
                $result = $this->asrama->edit_asrama($dataInfo, $arr, $arr_target);
            }
            echo json_encode($result);
            redirect(base_url('/asrama/index'));
        } else {
            echo json_encode(array(
                "status"=> 400,
                "message"=> "request body tidak sesuai dengan form validation",
            ));
        }
    }

    public function add_perbaikan() 
    {
        $data = []; 
        $data['judul'] = 'Add Perbaikan Asset';
        $kode = $this->input->post('kode', true);

        //form validation
        $validation = $this->form_validation_history();

        //upload file
        $dataInfo = $this->upload_file();

        if ($validation == true) {
            $result = $this->asset->add_history($kode, $dataInfo);
            echo json_encode($result);
            redirect(base_url('/asrama/index'));
        } else {
            echo json_encode(array(
                "status"=> 400,
                "message"=> validation_errors(),
            ));
        }
    }
}