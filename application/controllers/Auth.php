<?php 

require_once(APPPATH."controllers/Asset.php");

class Auth extends Asset
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model', 'auth');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    public function index(){
        if ($this->session->userdata('username') && $this->session->userdata('role') == 'admin') {
            $this->load->view('templates/header');
            $this->load->view('users/index');
            $this->load->view('templates/footer');
        } else {
            redirect('login/index');
        }
    }

    private function _login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $auth = $this->db->get_where("auth", ['username' => $username])->row_array();
        
        if ($auth) {
            //check password
            if (password_verify($password, $auth['password'])) {
                $data_auth = $this->asset->get_auth_by_username($username)->row();
                $data = [
                    'username' => $auth['username'],
                    'role' => $data_auth->role,
                    'id_user' => $data_auth->id_user
                ];
                $this->session->set_userdata($data);
                if ($data_auth->role == 'admin') {
                    redirect('home/index');
                } else {
                    redirect('home/user');
                }
            } else {
                //password salah
                $error = json_encode(array(
                    "message" => "password is incorrect",
                ));
                var_dump($error);
                //redirect('login/index');
            }
        } else {
            //redirect 404
            $error = json_encode(array(
                "status" => 404,
                "message" => "username is not registered",
            ));
            var_dump($error);
            //redirect('login/index');
        }
    }

    public function login(){
        
        $this->form_validation->set_rules('username', 'username', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
    
        if ($this->form_validation->run() == false) {
            $this->load->view('login/index');
        } 
        else {
            $this->_login();
        }
    }

    public function logout(){
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('role');
        $this->session->unset_userdata('id_user');

        redirect('login/index');
    }

    private function form_validation_pengguna_aset()
    {
        $inputString = ['kode_asset', 'nama', 'departemen'];
        $inputNumber = ['nik'];

        $this->validation($inputString, 'trim|required');
        $this->validation($inputNumber, 'trim|required|numeric|min_length[16]');

        if ($this->form_validation->run() == TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private function form_validation_registrasi()
    {
        $inputUsername = ['username'];
        $inputPassword = ['password'];
        $inputPassword1 = ['password2'];

        $this->validation($inputUsername, 'trim|required|is_unique[auth.username]|min_length[5]');
        $this->validation($inputPassword, 'trim|required|min_length[5]|matches[password2]');
        $this->validation($inputPassword1, 'trim|required|matches[password]');

        if ($this->form_validation->run() == TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //method yang digunakan untuk request data mahasiswa
    public function fetch_asset_user()
    {
        header('Content-Type: application/json');
        $list = $this->auth->get_datatables_user();
        $data = array();
        $no = $this->input->post('start');
        //looping data mahasiswa
        foreach ($list as $Data_user) {
            $no++;
            $row = array();
            //row pertama akan kita gunakan untuk btn edit dan delete
            $auth = $this->asset->get_user_asset_deleted($Data_user->id_user)->row();
            $row[] = $auth->nik;
            $row[] = $auth->nama;
            $row[] = $auth->departemen;
            
            $row[] = $Data_user->kategori;
            $asset = $this->asset->get_asset($Data_user->kode_asset, 'asset')->row();
            $row[] = $asset->nama;
            $row[] = $Data_user->kode_asset;
            $row[] = $Data_user->lokasi;
            
            $row[] = $auth->file;
            $row[] = '<a class="btn btn-danger btn-sm " name="deleteButton" data-id="' . $Data_user->id_user . '" data-bs-toggle="modal" data-bs-target="#delete_user" onclick="deleteClick(this)"><i class="bi bi-trash"></i></a>
                      <a class="btn btn-warning btn-sm " name="addRegistButton" data-id="' . $auth->nik . '" data-bs-toggle="modal" data-bs-target="#add_regist" onclick="AddRegist(this)"><i class="bi bi-info"></i></a>';
            
            $data[] = $row;
        }
        
        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->auth->count_all(),
            "recordsFiltered" => $this->auth->count_filtered_user(),
            "data" => $data,
        );

        //output to json format
        $this->output->set_output(json_encode($output));
    }

    public function add_pengguna_asset()
    {
        $data = [];
        $dataInfo = [];
        $data['judul'] = 'Add Pengguna Asset';

        //form validation
        $validation = $this->form_validation_pengguna_aset();

        //upload file
        $dataInfo = $this->upload_file();

        //generate id user
        $nomor = $this->asset->get_count('user') + 1;
        $tgl_pengadaan = date("Y-m-d",time());
        $kode = strtoupper($tgl_pengadaan . '/' . 'U' . '/' . 'UBS' . '/' . $nomor);

        if ($validation == FALSE) {
            echo json_encode(array(
                "status" => 400,
                "message" => validation_errors(),
            ));
        } else {
            $result = $this->auth->add_user($dataInfo, $kode);
            echo json_encode($result);
        }
    }

    public function registrasi()
    {
        $data = [];
        $data['judul'] = 'Add Regist';

        //form validation
        $validation = $this->form_validation_registrasi();

        //generate id regist
        $nomor = $this->asset->get_count('auth') + 1;
        $tgl_pengadaan = date("Y-m-d",time());
        $kode = strtoupper($tgl_pengadaan . '/' . 'RE' . '/' . 'UBS' . '/' . $nomor);

        if ($validation == FALSE) {
            echo json_encode(array(
                "status" => 400,
                "message" => validation_error(),
            ));
        } else {
            $result = $this->auth->add_regist($kode, $this->input->post('nik', true));
            echo json_encode($result);
            redirect(base_url('/auth/index'));
        }
    }

    public function delete_pengguna()
    {
        $data = [];
        $data['judul'] = 'Delete Pengguna Aset';
        $this->form_validation->set_rules('id_user', 'id_user', 'trim|required');
        if ($this->form_validation->run() == TRUE) {
            $id_user = $this->input->post('id_user', true);
            if ($this->asset->get_user_count_by_id($id_user) > 0) {
                $result = $this->auth->delete_user($id_user, date("Y-m-d",time()));
                if ($result == 1) {
                    $auth = $this->auth->get_json_user($id_user);
                    echo json_encode($auth);
                    redirect(base_url('/auth/index'));
                    //redirect(base_url('/auth/index'));
                } else {
                    echo json_encode($result);
                    redirect(base_url('/auth/index'));
                    //redirect(base_url('/auth/index'));
                } 
            } else {
                $error = array(
                    'error_code' => '404',
                    'detail' => 'data not found or data has been deleted',
                    'kode' => $id_user
                );
    
                echo json_encode($error);
            }
        } else {
            $error = array(
                'error_code' => '400',
                'detail' => 'no kode is given'
            );

            echo json_encode($error);
        }
    }
}
