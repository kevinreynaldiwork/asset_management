<?php 

require_once(APPPATH . "models/Asset_model.php");

class Auth_model extends Asset_model
{
    //set nama tabel yang akan kita tampilkan datanya
    var $table = 'asset';
    
    var $column_order_user = array('user.nik', 'user.nama', 'user.departemen', 'asset.kategori', 'asset.nama', 'asset.kode_asset', 'asset.lokasi', 'user.file', null);

    var $column_search_user = array('user.nik', 'user.nama', 'user.departemen', 'asset.kategori', 'asset.nama', 'asset.kode_asset', 'asset.lokasi', 'user.file');

    var $order_user = array('user.nik' => 'asc');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('Asset_model', 'asset');
    }

    private function _get_datatables_query_user()
    {
        $this->db->from($this->table);
        $this->db->join('user', 'user.kode_asset = asset.kode_asset');
        $i = 0;
        foreach ($this->column_search_user as $item) // loop kolom 
        {
            if (isset($this->input->post('search')['value'])) // jika datatable mengirim POST untuk search
            {
                if ($i === 0) // looping pertama
                {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('search')['value']);
                } else {
                    $this->db->or_like($item, $this->input->post('search')['value']);
                }
                if (count($this->column_search_user) - 1 == $i) //looping terakhir
                    $this->db->group_end();
            }
            $i++;
        }

        // jika datatable mengirim POST untuk order
        if ($this->input->post('order')) {
            $this->db->order_by($this->column_order_user[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
        } else if (isset($this->order)) {
            $order_db = $this->order_user;
            $this->db->order_by(key($order_db), $order_db[key($order_db)]);
        }
    }

    function get_datatables_user()
    {
        $this->_get_datatables_query_user();
        if ($this->input->post('length') != -1)
            $this->db->limit($this->input->post('length'), $this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_user()
    {
        $this->_get_datatables_query_user();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_json_user($kode)
    {
        $user = $this->asset->get_user_asset_deleted($kode)->row();
        
        if (isset($user)) {
            $asset = $this->asset->get_asset($user->kode_asset, 'asset')->row();

            $asset_json = array(
                'id_user' => $kode,
                'nik' => $user->nik,
                'nama' => $user->nama,
                'departemen' => $user->departemen,
                'file' => $user->file,
                'kode_asset' => $user->kode_asset,
                'nama_asset' => $asset->nama,
                'lokasi' => $asset->lokasi,
                'kategori' => $asset->kategori,
            );

            return $asset_json;
        } else {
            $error = array(
                'error_code' => '404',
                'detail' => 'data not found or data has been deleted',
                'kode' => $kode
            );
    
            echo json_encode($error);

            return $error;
        }
    }

    public function get_json_auth($id_auth, $id_user)
    {
        $auth = $this->asset->get_auth($id_auth)->row();
        
        if ($auth) {
            $auth_json = array(
                'id_auth' => $id_auth,  
                'username' => $auth->username,  
                'password' => $auth->password,  
                'role' => $auth->role,
                'id_user' => $id_user,  
            );

            return $auth_json;
        } else {
            $error = array(
                'error_code' => '404',
                'detail' => 'data not found or data has been deleted',
                'id_auth' => $id_auth
            );
    
            return $error;
        }
    }
    
    private function prep_user($dataInfo, $id_user)
    {
        $foto = [];
        foreach ($dataInfo as $file) {
            array_push($foto, $file['file_name']);
        }

        $data = [
            'id_user' => $id_user,
            'nik' => $this->input->post('nik', true),
            'nama' => strtolower($this->input->post('nama', true)),
            'departemen' => $this->input->post('departemen', true),
            'file' => json_encode($foto),
            'kode_asset' => $this->input->post('kode_asset', true)
        ];

        return $data;
    }

    private function prep_auth($kode , $id_user)
    {
        $data = [
            'id_auth' => $kode,
            'username' => htmlspecialchars($this->input->post('username', true)),
            'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            'role' => "user",   
            'id_user' => $id_user
        ];

        return $data;
    }

    public function add_user($dataInfo, $kode)
    {
        //insert user
        $this->db->db_debug = false;

        $user = $this->prep_user($dataInfo, $kode);

        if ($this->asset->get_user_count_by_kode($user['kode_asset']) == 0 && $this->asset->get_asset_count_by_kode($user['kode_asset']) >= 1) {
            $res = $this->db->insert('user', $user);

            //update asset
            $array_asset = array('kode_asset' => $user['kode_asset'], 'tanggal_delete' => null, 'alasan_delete' => null);
            $data = ['status' => 'in use'];
            $this->db->where($array_asset);
            $res1 = $this->db->update('asset', $data);

            if(!$res || !$res1) {
                return $this->db->error();
            } else {
                return $this->get_json_user($kode);
            }
        } else {
            $error = array(
                // 'error_code' => '404',
                'detail' => 'kode_asset has already been booked / used or kode has been deleted',
                'kode_asset' => $user['kode_asset']
            );
    
            return $error;
        }
    }

    public function add_regist($kode, $id_user)
    {
        //insert user
        $this->db->db_debug = false;

        if ($this->asset->get_auth_count_by_kode($id_user) == 0) {
            //insert auth regist
            $auth = $this->prep_auth($kode, $id_user);
            $res = $this->db->insert('auth', $auth);
        
            if(!$res) {
                return $this->db->error();
            } else {
                return $this->get_json_auth($kode, $id_user);
            }
        } else {
            $error = array(
                // 'error_code' => '404',
                'detail' => 'user already regist',
                'id_user' => $id_user
            );
    
            return $error;
        }
    }

    public function delete_user($kode, $tgl_delete)
    {
        $this->db->db_debug = false;
        $data = array(
            'tanggal_delete' => $tgl_delete,
            'alasan_delete' => $this->input->post('alasan_delete', true)
        );
        $this->db->where('id_user', $kode);
        $res = $this->db->update('user', $data);
        if(!$res) {
            return $this->db->error();
            //return array $error['code'] & $error['message']
        } else {
            return 1;
        }
    }
}
