<?php

require_once(APPPATH . "models/Asset_model.php");

class Rumah_model extends Asset_model
{
    //set nama tabel yang akan kita tampilkan datanya
    var $table = 'asset';
    //set kolom order, kolom pertama saya null untuk kolom edit dan hapus
    var $column_order_admin = array('kode_asset', 'nama', 'lokasi', 'tanggal_terima', 'status', null);

    var $column_search_admin = array('nama', 'lokasi', 'kode_asset', 'tanggal_terima', 'status');

    var $column_order_user = array('asset.kode_asset', 'asset.nama', 'asset.lokasi', 'asset.tanggal_terima', 'user.nama', 'user.departemen', 'asset.status');

    var $column_search_user = array('asset.nama', 'asset.lokasi', 'asset.kode_asset', 'asset.tanggal_terima', 'asset.status', 'user.nama', 'user.departemen');

    // default order 
    var $order = array('kode_asset' => 'asc');

    var $order_user = array('asset.kode_asset' => 'asc');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('Asset_model', 'asset');
    }

    public function _get_datatables_query_user()
    {
        // $array = array('asset.tanggal_delete' => null, 'asset.alasan_delete' => null);

        $this->db->from('user');
        $this->db->join('asset', 'user.kode_asset = asset.kode_asset', 'right');
        $this->db->where_in('kategori', 'rumah_dinas');

        $i = 0;
        foreach ($this->column_search_user as $item) // loop kolom 
        {   
            if (isset($this->input->post('search')['value'])) // jika datatable mengirim POST untuk search
            {
                if ($i === 0) // looping pertama
                {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('search')['value']);
                } else {
                    $this->db->or_like($item, $this->input->post('search')['value']);
                }
                if (count($this->column_search_user) - 1 == $i) //looping terakhir
                    $this->db->group_end();
            }
            $i++;
        }

        // jika datatable mengirim POST untuk order
        if ($this->input->post('order')) {
            $this->db->order_by($this->column_order_user[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
        } else if (isset($this->order)) {
            $order_db = $this->order_user;
            $this->db->order_by(key($order_db), $order_db[key($order_db)]);
        }
        //print_r($this->db->queries);
    }

    function get_datatables_user()
    {
        $this->_get_datatables_query_user();
        if ($this->input->post('length') != -1)
            $this->db->limit($this->input->post('length'), $this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_user()
    {
        $this->_get_datatables_query_user();
        $query = $this->db->get();
        return $query->num_rows();
    }

    private function _get_datatables_query_admin($kategori)
    {
        //$array = array('tanggal_delete' => null, 'alasan_delete' => null);

        //$this->db->where_in('kategori', $kategori)->where($array)->from($this->table);
        $this->db->where_in('kategori', $kategori)->from($this->table);
        $i = 0;
        foreach ($this->column_search_admin as $item) // loop kolom 
        {
            if (isset($this->input->post('search')['value'])) // jika datatable mengirim POST untuk search
            {
                if ($i === 0) // looping pertama
                {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('search')['value']);
                } else {
                    $this->db->or_like($item, $this->input->post('search')['value']);
                }
                if (count($this->column_search_admin) - 1 == $i) //looping terakhir
                    $this->db->group_end();
            }
            $i++;
        }

        // jika datatable mengirim POST untuk order
        if ($this->input->post('order')) {
            $this->db->order_by($this->column_order_admin[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query_admin('rumah_dinas');
        if ($this->input->post('length') != -1)
            $this->db->limit($this->input->post('length'), $this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query_admin('rumah_dinas');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_json_rumah_with_user($kode)
    {
        $asset = $this->asset->get_asset($kode, 'asset')->row();
        $detail = $this->asset->get_asset($kode, 'detail_asset')->row();
        $history = $this->asset->get_asset($kode, 'history_asset')->result_array();
        $user = $this->asset->get_user_by_kode($kode)->row();
        
        if ($asset && $detail && $user) {
            $kegiatan = [];

            $asset_json = array(
                'kode_asset' => $kode,
                'nama' => $asset->nama,
                'lokasi' => $asset->lokasi,
                'kategori' => $asset->kategori,
                'status' => $asset->status,
                'tanggal_terima' => $asset->tanggal_terima,
                'id_user' => $user->id_user,
                'id_detail' => $detail->id_detail,
                'info_asset' => $detail->info_asset,
                'foto_asset' => $detail->foto_asset,
            );

            if (isset($history) && count($history) > 0) {
                foreach ($history as $his) {
                    array_push($kegiatan, array(
                        'id_history' => $his['id_history'],
                        'tanggal_kegiatan' => $his['tanggal_kegiatan'],
                        'kegiatan' => $his['kegiatan'],
                        'detail_kegiatan' => $his['detail_kegiatan']
                    ));
                }

                $asset_json = array_merge($asset_json, array(
                    'history' => $kegiatan
                ));
            }
            return $asset_json;
        } else {
            $error = array(
                'error_code' => '404',
                'detail' => 'data not found or data has been deleted',
                'kode' => $kode
            );
    
            echo json_encode($error);
        }
    }

    public function get_json_rumah($kode)
    {
        $asset = $this->asset->get_asset($kode, 'asset')->row();
        $detail = $this->asset->get_asset($kode, 'detail_asset')->row();
        $history = $this->asset->get_asset($kode, 'history_asset')->result_array();
        
        if ($asset && $detail) {
            $kegiatan = [];

            $asset_json = array(
                'kode_asset' => $kode,
                'nama' => $asset->nama,
                'lokasi' => $asset->lokasi,
                'kategori' => $asset->kategori,
                'status' => $asset->status,
                'tanggal_terima' => $asset->tanggal_terima,
                'id_detail' => $detail->id_detail,
                'info_asset' => $detail->info_asset,
                'foto_asset' => $detail->foto_asset,
            );

            if (isset($history) && count($history) > 0) {
                foreach ($history as $his) {
                    array_push($kegiatan, array(
                        'id_history' => $his['id_history'],
                        'tanggal_kegiatan' => $his['tanggal_kegiatan'],
                        'kegiatan' => $his['kegiatan'],
                        'detail_kegiatan' => $his['detail_kegiatan']
                    ));
                }

                $asset_json = array_merge($asset_json, array(
                    'history' => $kegiatan
                ));
            }
            return $asset_json;
        } else {
            $error = array(
                'error_code' => '404',
                'detail' => 'data not found or data has been deleted',
                'kode' => $kode
            );
    
            echo json_encode($error);
        }
    }

    private function prep_asset()
    {
        $asset = array(
            'nama' => $this->input->post('nama', true),
            'lokasi' => $this->input->post('lokasi', true),
            'kategori' => 'rumah_dinas',
            'status' => 'available',
            'tanggal_terima' => $this->input->post('tgl_pengadaan', true)
        );

        return $asset;
    }

    private function prep_detail_asset($dataInfo)
    {
        $foto = [];

        $info = array(
            'jenis' => $this->input->post('jenis', true),
            'kondisi' => $this->input->post('kondisi', true),
            'carport' => $this->input->post('carport', true),
            'fasilitas' => $this->input->post('fasilitas', true),
            'kamar_mandi' => $this->input->post('kamar_mandi', true),
            'kamar_tidur' => $this->input->post('kamar_tidur', true),
        );

        if ($dataInfo == "") {
            $detail = array(
                'info_asset' => json_encode($info),
            );
        } else {
            if (is_array($dataInfo) || is_object($dataInfo)) {
                foreach ($dataInfo as $file) {
                    array_push($foto, $file['file_name']);
                }
    
                $detail = array(
                    'info_asset' => json_encode($info),
                    'foto_asset' => json_encode($foto),
                );
            } else {
                $arr=explode(",",$dataInfo);
            
                $detail = array(
                    'info_asset' => json_encode($info),
                    'foto_asset' => json_encode($arr),
                );
            }
        }
        
        return $detail;
    }

    public function add_rumah($dataInfo, $kode)
    {
        $id = $this->asset->get_count('detail_asset') + 1;

        $asset = $this->prep_asset();
        $asset_json = array_merge($asset, array(
            'kode_asset' => $kode
        ));

        $detail = $this->prep_detail_asset($dataInfo);
        $detail_json = array_merge($detail, array(
            'id_detail' => 'D_' . $id . '/' . substr($kode, 11),
            'kode_asset' => $kode,
        ));

        $this->db->db_debug = false;

        //insert asset and detail asset
        $res = $this->db->insert('asset', $asset_json);
        $res1 = $this->db->insert('detail_asset', $detail_json);

        if(!$res || !$res1) {
            return $this->db->error();
        } else {
            return $this->get_json_rumah($kode);
        }
    }

    public function edit_rumah($dataInfo)
    {
        $asset = $this->prep_asset($dataInfo);

        $detail = $this->prep_detail_asset($dataInfo);

        $this->db->db_debug = false;
        //update asset
        $array_asset = array('kode_asset' => $this->input->post('kode'), 'tanggal_delete' => null, 'alasan_delete' => null);
        $this->db->where($array_asset);
        $this->db->update('asset', $asset);

        //update detail_asset
        $detail_asset = $this->asset->get_asset($this->input->post('kode'), 'detail_asset')->row();
        $detail_id = $detail_asset->id_detail;
        $array_detail = array('kode_asset' => $this->input->post('kode'), 'id_detail' => $detail_id);
        $this->db->where($array_detail);
        $res = $this->db->update('detail_asset', $detail);

        if(!$res) {
            return $this->db->error();
        } else {
            return $this->get_json_rumah($this->input->post('kode'));
        }
    }
}
