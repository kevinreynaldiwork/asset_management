<?php

class Asset_model extends CI_Model
{
    //set nama tabel yang akan kita tampilkan datanya
    var $table = 'history_asset';
    
    var $column_order_user = array('id_history', 'tanggal_kegiatan', 'kegiatan');

    var $column_search_user = array('id_history', 'tanggal_kegiatan', 'kegiatan');

    var $order_user = array('id_history' => 'asc');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_non_deleted_asset($kode, $table)
    {
        $array = array('kode_asset' => $kode, 'tanggal_delete' => null, 'alasan_delete' => null);
        $this->db->where($array);
        return $this->db->get($table);
    }

    public function get_deleted_asset($kode, $table)
    {
        $array = array('kode_asset' => $kode, 'tanggal_delete !=' => null, 'alasan_delete !=' => null);
        $this->db->where($array);
        return $this->db->get($table);
    }

    public function get_asset($kode, $table)
    {
        $array = array('kode_asset' => $kode);
        $this->db->where($array);
        return $this->db->get($table);
    }

    public function get_asset_count($kategori)
    {
        if (is_array($kategori)) {
            $this->db->where_in('kategori', $kategori);
        } else {
            $this->db->where('kategori', $kategori);
        }

        $this->db->from('asset');
        return $this->db->count_all_results();
    }

    public function get_count_non_deleted($kategori)
    {
        if (is_array($kategori)) {
            $array = array('tanggal_delete' => null, 'alasan_delete' => null);
            $this->db->where_in('kategori', $kategori)->where($array);
            // $sql = "SELECT * FROM asset WHERE kategori IN ? AND tanggal_delete = ? AND alasan_delete = ?";
            // $this->db->query($sql, array($kategori, null, null));
        } else {
            $array = array('kategori' => $kategori, 'tanggal_delete' => null, 'alasan_delete' => null);
            $this->db->where($array);
        }

        $query = $this->db->get('asset');
        return $query->num_rows();
    }

    public function get_user_asset($id_user)
    {
        $array = array('id_user' => $id_user, 'tanggal_delete' => null, 'alasan_delete' => null);
        $this->db->where($array);
        return $this->db->get('user');
    }

    public function get_user_asset_deleted($id_user)
    {
        $array = array('id_user' => $id_user);
        $this->db->where($array);
        return $this->db->get('user');
    }

    public function get_auth($id_user)
    {
        $array = array('id_auth' => $id_user);
        $this->db->where($array);
        return $this->db->get('auth');
    }

    public function get_auth_by_username($username)
    {
        $array = array('username' => $username);
        $this->db->where($array);
        return $this->db->get('auth');
    }

    public function get_user_by_kode($kode)
    {
        $array = array('kode_asset' => $kode, 'tanggal_delete' => null, 'alasan_delete' => null);
        $this->db->where($array);
        return $this->db->get('user');
    }

    public function get_asset_count_by_kode($kode)
    {
        $array = array('kode_asset' => $kode, 'tanggal_delete' => null, 'alasan_delete' => null);
        $this->db->where($array);
        $this->db->from('asset');
        return $this->db->count_all_results();
    }

    public function get_user_count_by_kode($kode)
    {
        $array = array('kode_asset' => $kode, 'tanggal_delete' => null, 'alasan_delete' => null);
        $this->db->where($array);
        $this->db->from('user');
        return $this->db->count_all_results();
    }

    public function get_user_count_by_id($kode)
    {
        $array = array('id_user' => $kode, 'tanggal_delete' => null, 'alasan_delete' => null);
        $this->db->where($array);
        $this->db->from('user');
        return $this->db->count_all_results();
    }

    public function get_auth_count_by_kode($kode)
    {
        $array = array('id_user' => $kode);
        $this->db->where($array);
        $this->db->from('auth');
        return $this->db->count_all_results();
    }

    private function _get_datatables_query_history($kode)
    {
        $this->db->where_in('kode_asset', $kode)->from($this->table);
        $i = 0;
        foreach ($this->column_search_user as $item) // loop kolom 
        {
            if (isset($this->input->post('search')['value'])) // jika datatable mengirim POST untuk search
            {
                if ($i === 0) // looping pertama
                {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('search')['value']);
                } else {
                    $this->db->or_like($item, $this->input->post('search')['value']);
                }
                if (count($this->column_search_user) - 1 == $i) //looping terakhir
                    $this->db->group_end();
            }
            $i++;
        }

        // jika datatable mengirim POST untuk order
        if ($this->input->post('order')) {
            $this->db->order_by($this->column_order_user[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
        } else if (isset($this->order)) {
            $order_db = $this->order_user;
            $this->db->order_by(key($order_db), $order_db[key($order_db)]);
        }
    }

    function get_datatables_history($kode)
    {
        $this->_get_datatables_query_history($kode);
        if ($this->input->post('length') != -1)
            $this->db->limit($this->input->post('length'), $this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_history($kode)
    {
        $this->_get_datatables_query_history($kode);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_count($table)
    {
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    public function get_list_kode_asset()
    {
        $array = array('tanggal_delete' => null, 'alasan_delete' => null);
        $this->db->where($array);
        return $this->db->get('asset');
    }

    public function get_last_asset($kategori)
    {
        $this->db->from('asset');
        return $this->db->count_all_results();
    }

    public function delete_asset($kode, $tgl_delete)
    {
        $this->db->db_debug = false;
        $data = array(
            'status' => 'deleted',
            'tanggal_delete' => $tgl_delete,
            'alasan_delete' => $this->input->post('alasan_delete', true)
        );
        $this->db->where('kode_asset', $kode);
        $res = $this->db->update('asset', $data);
        if(!$res) {
            return $this->db->error();
            //return array $error['code'] & $error['message']
        } else {
            return 1;
        }
    }

    public function get_json_history($kode)
    {
        $history = $this->asset->get_asset($kode, 'history_asset')->result_array();
        $kegiatan = [];

        if (isset($history)) {
            foreach ($history as $his) {
                array_push($kegiatan, array(
                    'id_history' => $his['id_history'],
                    'tanggal_kegiatan' => $his['tanggal_kegiatan'],
                    'kegiatan' => $his['kegiatan'],
                    'detail_kegiatan' => $his['detail_kegiatan']
                ));
            }
    
            return $kegiatan;
        } else {
            $error = array(
                'error_code' => '404',
                'detail' => 'data not found or data has been deleted',
                'kode' => $kode
            );
    
            return $error;
        }
    }

    public function get_detail_history($id_history)
    {
        $array = array('id_history' => $id_history);
        $this->db->where($array);
        return $this->db->get('history_asset');
    }

    public function get_json_detail_history($id_history)
    {
        $detail_history = $this->asset->get_detail_history($id_history)->row();
        
        if (isset($detail_history)) {
            $json = json_decode($detail_history->detail_kegiatan,true);

            $detail_kegiatan = [
                'id_history' => $detail_history->id_history,
                'tanggal_kegiatan' => $detail_history->tanggal_kegiatan,
                'kronologi' => $json['kronologi'],
                'kondisi' => $json['kondisi'],
                'action_plan' => $json['action_plan'],
                'RAB' => $json['RAB'],
                'foto_kegiatan' => $detail_history->foto_kegiatan
            ];

            return $detail_kegiatan;
        } else {
            $error = array(
                'error_code' => '404',
                'detail' => 'data not found or data has been deleted',
                'kode' => $id_history
            );
    
            return $error;
        }
    }

    public function add_history($kode, $dataInfo)
    {
        $foto = [];
        $id = $this->get_count('history_asset') + 1;

        foreach ($dataInfo as $file) {
            array_push($foto, $file['file_name']);
        }

        $detail = array(
            'kronologi' => $this->input->post('kronologi', true),
            'kondisi' => $this->input->post('kondisi', true),
            'action_plan' => $this->input->post('action_plan', true),
            'RAB' => $this->input->post('RAB', true),
        );

        $history = array(
            'id_history' => 'H_' . $id . '/' . substr($kode, 11),
            'kode_asset' => $kode,
            'tanggal_kegiatan' => $this->input->post('tgl_kejadian', true),
            'kegiatan' => 'perbaikan',
            'detail_kegiatan' => json_encode($detail),
            'foto_kegiatan' => json_encode($foto),
        );

        $this->db->db_debug = false;

        $res = $this->db->insert('history_asset', $history);

        if(!$res) {
            return $this->db->error();
        } else {
            return $this->get_json_detail_history($history['id_history']);
        }
    }
}
