<?php

require_once(APPPATH . "models/Asset_model.php");

class Asrama_model extends Asset_model
{
    //set nama tabel yang akan kita tampilkan datanya
    var $table = 'asset';
    
    //set kolom order, kolom pertama saya null untuk kolom edit dan hapus
    var $column_order_admin = array('asset.nama', 'detail_asset.info_asset', 'detail_asset.info_asset', 'detail_asset.info_asset', 'asset.status', null);

    var $column_search_admin = array('asset.nama', 'asset.status', 'detail_asset.info_asset');

    var $column_order_user = array('asset.nama', 'detail_asset.info_asset', 'detail_asset.info_asset', 'detail_asset.info_asset', 'asset.status', null);

    var $column_search_user = array('asset.nama', 'asset.status', 'detail_asset.info_asset');

    // default order 
    var $order = array('asset.kode_asset' => 'asc');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('Asset_model', 'asset');
    }

    private function _get_datatables_query_user($kategori)
    {
        // $array = array('tanggal_delete' => null, 'alasan_delete' => null);

        $this->db->where_in('asset.kategori', $kategori)->from($this->table);
        $this->db->join('detail_asset', 'asset.kode_asset = detail_asset.kode_asset');

        $i = 0;
        foreach ($this->column_search_user as $item) // loop kolom 
        {
            if (isset($this->input->post('search')['value'])) // jika datatable mengirim POST untuk search
            {
                if ($i === 0) // looping pertama
                {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('search')['value']);
                } else {
                    $this->db->or_like($item, $this->input->post('search')['value']);
                }
                if (count($this->column_search_user) - 1 == $i) //looping terakhir
                    $this->db->group_end();
            }
            $i++;
        }

        // jika datatable mengirim POST untuk order
        if ($this->input->post('order')) {
            $this->db->order_by($this->column_search_user[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_user()
    {
        $this->_get_datatables_query_user('asrama');
        if ($this->input->post('length') != -1)
            $this->db->limit($this->input->post('length'), $this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_user()
    {
        $this->_get_datatables_query_user('asrama');
        $query = $this->db->get();
        return $query->num_rows();
    }

    private function _get_datatables_query_admin($kategori)
    {
        //$array = array('tanggal_delete' => null, 'alasan_delete' => null);

        //$this->db->where_in('kategori', $kategori)->where($array)->from($this->table);
        $this->db->where_in('asset.kategori', $kategori)->from($this->table);
        $this->db->join('detail_asset', 'asset.kode_asset = detail_asset.kode_asset');

        $i = 0;
        foreach ($this->column_search_admin as $item) // loop kolom 
        {
            if (isset($this->input->post('search')['value'])) // jika datatable mengirim POST untuk search
            {
                if ($i === 0) // looping pertama
                {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('search')['value']);
                } else {
                    $this->db->or_like($item, $this->input->post('search')['value']);
                }
                if (count($this->column_search_admin) - 1 == $i) //looping terakhir
                    $this->db->group_end();
            }
            $i++;
        }

        // jika datatable mengirim POST untuk order
        if ($this->input->post('order')) {
            $this->db->order_by($this->column_order_admin[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query_admin('asrama');
        if ($this->input->post('length') != -1)
            $this->db->limit($this->input->post('length'), $this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query_admin('asrama');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_json_asrama($kode)
    {
        $asset = $this->asset->get_asset($kode, 'asset')->row();
        $detail = $this->asset->get_asset($kode, 'detail_asset')->row();
        $history = $this->asset->get_asset($kode, 'history_asset')->result_array();

        if ($asset && $detail) {
            $kegiatan = [];

            $asset_json = array(
                'kode_asset' => $kode,
                'nama' => $asset->nama,
                'lokasi' => $asset->lokasi,
                'kategori' => $asset->kategori,
                'status' => $asset->status,
                'tanggal_terima' => $asset->tanggal_terima,
                'id_detail' => $detail->id_detail,
                'info_asset' => $detail->info_asset,
                'foto_asset' => $detail->foto_asset,
                'fasilitas' => $detail->fasilitas,
                'jumlah_fasilitas' => $detail->jumlah_fasilitas
            );

            if (isset($history) && count($history) > 0) {
                foreach ($history as $his) {
                    array_push($kegiatan, array(
                        'id_history' => $his['id_history'],
                        'tanggal_kegiatan' => $his['tanggal_kegiatan'],
                        'kegiatan' => $his['kegiatan'],
                        'detail_kegiatan' => $his['detail_kegiatan']
                    ));
                }

                $asset_json = array_merge($asset_json, array(
                    'history' => $kegiatan
                ));
            }
            return $asset_json;
        } else {
            $error = array(
                'error_code' => '404',
                'detail' => 'data not found or data has been deleted',
                'kode' => $kode
            );
    
            echo json_encode($error);
        }
    }

    private function prep_asset()
    {
        $asset = array(
            'nama' => $this->input->post('asrama', true),
            'lokasi' => 'none',
            'kategori' => 'asrama',
            'status' => 'available',
            'tanggal_terima' => $this->input->post('tgl_pengadaan', true)
        );

        return $asset;
    }

    private function prep_detail_asset($dataInfo, $fasilitas, $jumlah_fasilitas)
    {
        $foto = [];

        $info = array(
            'lantai' => $this->input->post('lantai', true),
            'kamar' => $this->input->post('kamar', true),
            'penghuni' => $this->input->post('penghuni', true),
            'kapasitas' => $this->input->post('kapasitas', true)
        );

        if ($dataInfo == "") {
            $detail = array(
                'info_asset' => json_encode($info),
                'fasilitas' => json_encode($fasilitas),
                'jumlah_fasilitas' => json_encode($jumlah_fasilitas)
            );
        } else {
            if (is_array($dataInfo) || is_object($dataInfo)) {
                foreach ($dataInfo as $file) {
                    array_push($foto, $file['file_name']);
                }
    
                $detail = array(
                    'info_asset' => json_encode($info),
                    'foto_asset' => json_encode($foto),
                    'fasilitas' => json_encode($fasilitas),
                    'jumlah_fasilitas' => json_encode($jumlah_fasilitas)
                );
            } else {
                $arr=explode(",",$dataInfo);
            
                $detail = array(
                    'info_asset' => json_encode($info),
                    'foto_asset' => json_encode($arr),
                    'fasilitas' => json_encode($fasilitas),
                    'jumlah_fasilitas' => json_encode($jumlah_fasilitas)
                );
            }
        }
        
        return $detail;
    }

    public function add_asrama($dataInfo, $kode, $fasilitas, $jumlah_fasilitas)
    {

        $id = $this->asset->get_count('detail_asset') + 1;

        $asset = $this->prep_asset($dataInfo);
        $asset_json = array_merge($asset, array(
            'kode_asset' => $kode
        ));

        $detail = $this->prep_detail_asset($dataInfo, $fasilitas, $jumlah_fasilitas);
        $detail_json = array_merge($detail, array(
            'id_detail' => 'D_' . $id . '/' . substr($kode, 11),
            'kode_asset' => $kode,
        ));

        $this->db->db_debug = false;

        //insert asset and detail asset
        $res = $this->db->insert('asset', $asset_json);
        $res1 = $this->db->insert('detail_asset', $detail_json);

        if(!$res || !$res1) {
            return $this->db->error();
        } else {
            return $this->get_json_asrama($kode);
        }
    }

    public function edit_asrama($dataInfo, $fasilitas, $jumlah_fasilitas)
    {
        $asset = $this->prep_asset();

        $detail = $this->prep_detail_asset($dataInfo, $fasilitas, $jumlah_fasilitas);

        $this->db->db_debug = false;

        //update asset
        $array_asset = array('kode_asset' => $this->input->post('kode'), 'tanggal_delete' => null, 'alasan_delete' => null);
        $this->db->where($array_asset);
        $this->db->update('asset', $asset);

        //update detail_asset
        $detail_asset = $this->asset->get_asset($this->input->post('kode'), 'detail_asset')->row();
        $detail_id = $detail_asset->id_detail;
        $array_detail = array('kode_asset' => $this->input->post('kode'), 'id_detail' => $detail_id);
        $this->db->where($array_detail);
        $res = $this->db->update('detail_asset', $detail);

        if(!$res) {
            return $this->db->error();
        } else {
            return $this->get_json_asrama($this->input->post('kode'));
        }
    }
}
