-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 23, 2022 at 06:05 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asset`
--

-- --------------------------------------------------------

--
-- Table structure for table `asset`
--

CREATE TABLE `asset` (
  `kode_asset` varchar(50) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `lokasi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `kategori` enum('rumah_dinas','gedung','mobil','motor','fasilitas','asrama') DEFAULT NULL,
  `status` varchar(50) NOT NULL,
  `tanggal_terima` date NOT NULL,
  `tanggal_delete` date DEFAULT NULL,
  `alasan_delete` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `asset`
--

INSERT INTO `asset` (`kode_asset`, `nama`, `lokasi`, `kategori`, `status`, `tanggal_terima`, `tanggal_delete`, `alasan_delete`) VALUES
('2022-03-28/A/UBS/1', 'Glory_Lama', 'none', 'asrama', 'available', '2022-03-28', NULL, NULL),
('2022-03-28/F/UBS/1', 'LCD Proyektor Coi', 'malang', 'fasilitas', 'available', '2022-03-28', NULL, NULL),
('2022-03-28/F/UBS/2', 'Scanner', 'surabaya', 'fasilitas', 'available', '2022-03-28', NULL, NULL),
('2022-03-28/G/UBS/1', 'Gedung A lt. 2', 'surabaya', 'gedung', 'available', '2022-03-28', NULL, NULL),
('2022-03-28/G/UBS/2', 'Gedung B lt. 1', 'surabaya', 'gedung', 'available', '2022-03-28', NULL, NULL),
('2022-03-28/K/UBS/1', 'Volkswagen', 'Surabaya', 'mobil', 'available', '2022-03-28', NULL, NULL),
('2022-03-28/K/UBS/2', 'Alphard', 'malang', 'mobil', 'deleted', '2022-03-28', '2022-04-05', 'coba'),
('2022-03-28/K/UBS/3', 'Alphard', 'surabaya', 'mobil', 'available', '2022-03-28', NULL, NULL),
('2022-03-28/R/UBS/1', 'rumah dinas 8 * 12', 'jakarta', 'rumah_dinas', 'available', '2022-03-28', NULL, NULL),
('2022-03-29/G/UBS/3', 'Gedung B lt. 1', 'jakarta', 'gedung', 'available', '2022-03-28', NULL, NULL),
('2022-03-30/K/UBS/4', 'Toyota', 'jakarta', 'motor', 'available', '2022-03-30', NULL, NULL),
('2022-07-17/R/UBS/2', 'rumah dinas 5 * 6', 'bandung', 'rumah_dinas', 'available', '2022-07-17', NULL, NULL),
('2022-07-17/R/UBS/3', 'rumah dinas jordan', 'kenjeran', 'rumah_dinas', 'deleted', '2022-07-17', '2022-07-17', 'coba');

-- --------------------------------------------------------

--
-- Table structure for table `auth`
--

CREATE TABLE `auth` (
  `id_auth` varchar(128) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('admin','user') NOT NULL,
  `id_user` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `detail_asset`
--

CREATE TABLE `detail_asset` (
  `id_detail` varchar(50) NOT NULL,
  `kode_asset` varchar(100) NOT NULL,
  `info_asset` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`info_asset`)),
  `fasilitas` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `jumlah_fasilitas` longtext DEFAULT NULL,
  `foto_asset` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`foto_asset`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `detail_asset`
--

INSERT INTO `detail_asset` (`id_detail`, `kode_asset`, `info_asset`, `fasilitas`, `jumlah_fasilitas`, `foto_asset`) VALUES
('D_1/R/UBS/1', '2022-03-28/R/UBS/1', '{\"jenis\":\"hai\",\"kondisi\":\"elek\",\"carport\":\"ada\",\"fasilitas\":\"mesin cuci\",\"kamar_mandi\":\"2\",\"kamar_tidur\":\"4\"}', NULL, NULL, '[\"SpringRoll.jpg\"]'),
('D_12/R/UBS/2', '2022-07-17/R/UBS/2', '{\"jenis\":\"Tetap\",\"kondisi\":\"Baik\",\"carport\":\"Ada\",\"fasilitas\":\"sofa\",\"kamar_mandi\":\"2\",\"kamar_tidur\":\"3\"}', NULL, NULL, '[\"ganyu.jpg\"]'),
('D_13/R/UBS/3', '2022-07-17/R/UBS/3', '{\"jenis\":\"Tetap\",\"kondisi\":\"Rusak\",\"carport\":\"Ada\",\"fasilitas\":\"semua\",\"kamar_mandi\":\"2\",\"kamar_tidur\":\"4\"}', NULL, NULL, '[\"ganyu.jpg\"]'),
('D_14/A/UBS/1', '2022-03-28/A/UBS/1', '{\"lantai\":\"3\",\"kamar\":\"10\",\"penghuni\":\"5\",\"kapasitas\":\"20\"}', '[\"Kasur\",\" Lemari\",\" Meja belajar\",\"Kamar mandi \"]', '[\"5\",\" 8\",\" 10\",\"2\"]', '[\"resto22.jpg\"]'),
('D_14/G/UBS/3', '2022-03-29/G/UBS/3', '{\"peruntukan\":\"marketing\",\"gedung\":\"UBS - l\",\"jenis\":\"halo\",\"kondisi\":\"bagus\",\"carport\":\"ada\",\"fasilitas\":\"karpet, mesin cuci\",\"kamar_mandi\":\"5\",\"kamar_tidur\":\"10\"}', NULL, NULL, '[\"alice24.jpg\",\"buktiACC.PNG\",\"buktiyabb.png\"]'),
('D_15/K/UBS/4', '2022-03-30/K/UBS/4', '{\"jenis\":\"halo\",\"kondisi\":\"bagus\",\"nopol\":\"L 1431 EREN\",\"nomor_mesin\":\"HQ12E1234567\",\"durasi_pajak\":\"2022-06-20\",\"durasi_plat\":\"2023-04-02\"}', NULL, NULL, '[\"SoySauceChickenWithiRce.jpg\",\"SpringRoll.jpg\"]'),
('D_2/R/UBS/2', '2022-03-28/R/UBS/2', '{\"jenis\":\"halo\",\"kondisi\":\"bagus\",\"carport\":\"ada\",\"fasilitas\":\"karpet\",\"kamar_mandi\":\"2\",\"kamar_tidur\":\"4\"}', NULL, NULL, '[\"resto2.jpg\",\"resto3.jpeg\"]'),
('D_4/G/UBS/1', '2022-03-28/G/UBS/1', '{\"peruntukan\":\"hrd\",\"gedung\":\"UBS - l\",\"jenis\":\"halo\",\"kondisi\":\"bagus\",\"carport\":\"ada\",\"fasilitas\":\"karpet\",\"kamar_mandi\":\"2\",\"kamar_tidur\":\"4\"}', NULL, NULL, '[\"resto1.jpg\",\"SpringRoll.jpg\"]'),
('D_4/G/UBS/2', '2022-03-28/G/UBS/2', '{\"peruntukan\":\"marketing\",\"gedung\":\"UBS - l\",\"jenis\":\"halo\",\"kondisi\":\"bagus\",\"carport\":\"ada\",\"fasilitas\":\"karpet, mesin cuci\",\"kamar_mandi\":\"2\",\"kamar_tidur\":\"4\"}', NULL, NULL, '[\"resto1.jpg\"]'),
('D_5/K/UBS/1', '2022-03-28/K/UBS/1', '{\"jenis\":\"halo\",\"kondisi\":\"bagus\",\"nopol\":\"L 1430 WG\",\"nomor_mesin\":\"HQ12E123456\",\"durasi_pajak\":\"2022-06-20\",\"durasi_plat\":\"2023-04-02\"}', NULL, NULL, '[\"FriedChickenWithVegetablesSesameSauce.jpg\"]'),
('D_6/K/UBS/2', '2022-03-28/K/UBS/2', '{\"jenis\":\"halo\",\"kondisi\":\"bagus\",\"nopol\":\"L 1431 GAY\",\"nomor_mesin\":\"HQ12E1234567\",\"durasi_pajak\":\"2022-06-20\",\"durasi_plat\":\"2023-04-02\"}', NULL, NULL, '[\"resto1.jpg\",\"resto2.jpg\"]'),
('D_7/K/UBS/3', '2022-03-28/K/UBS/3', '{\"jenis\":\"halo\",\"kondisi\":\"bagus\",\"nopol\":\"L 1430 YAG\",\"nomor_mesin\":\"HQ12E123456\",\"durasi_pajak\":\"2022-06-20\",\"durasi_plat\":\"2023-04-02\"}', NULL, NULL, '[\"FriedChickenWithVegetablesSesameSauce.jpg\"]'),
('D_8/F/UBS/1', '2022-03-28/F/UBS/1', '{\"jenis\":\"LG\",\"kondisi\":\"bagus\",\"garansi\":\"berlaku hingga 2024\"}', NULL, NULL, '[\"SpringRoll.jpg\"]'),
('D_9/F/UBS/2', '2022-03-28/F/UBS/2', '{\"jenis\":\"brother\",\"kondisi\":\"bagus\",\"garansi\":\"berlaku hingga 2023\"}', NULL, NULL, '[\"resto1.jpg\",\"resto2.jpg\"]');

-- --------------------------------------------------------

--
-- Table structure for table `history_asset`
--

CREATE TABLE `history_asset` (
  `id_history` varchar(50) NOT NULL,
  `kode_asset` varchar(100) NOT NULL,
  `tanggal_kegiatan` date NOT NULL,
  `kegiatan` varchar(255) NOT NULL,
  `detail_kegiatan` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`detail_kegiatan`)),
  `foto_kegiatan` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `history_asset`
--

INSERT INTO `history_asset` (`id_history`, `kode_asset`, `tanggal_kegiatan`, `kegiatan`, `detail_kegiatan`, `foto_kegiatan`) VALUES
('H_1/R/UBS/1', '2022-03-28/R/UBS/1', '2022-03-24', 'perbaikan', '{\"kronologi\":\"halo\",\"kondisi\":\"hai\",\"action_plan\":\"ganti\",\"RAB\":\"EK001\",\"foto_kegiatan\":\"[\\\"alice26.jpg\\\",\\\"buktiyabb2.png\\\"]\"}', NULL),
('H_2/R/UBS/1', '2022-03-28/R/UBS/1', '2022-03-24', 'perbaikan', '{\"kronologi\":\"halo\",\"kondisi\":\"hai\",\"action_plan\":\"ganti\",\"RAB\":\"EK001\"}', '[\"alice27.jpg\",\"buktiyabb3.png\"]');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` varchar(128) NOT NULL,
  `nik` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `departemen` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `tanggal_delete` date DEFAULT NULL,
  `alasan_delete` varchar(255) DEFAULT NULL,
  `kode_asset` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nik`, `nama`, `departemen`, `file`, `tanggal_delete`, `alasan_delete`, `kode_asset`) VALUES
('2022-07-23/U/UBS/1', '3525015201880002', 'jordan', 'marketing', '[\"alice10.jpg\"]', NULL, NULL, '2022-03-28/R/UBS/1'),
('2022-07-23/U/UBS/2', '3525015201880002', 'jordan', 'marketing', '[\"alice12.jpg\"]', NULL, NULL, '2022-03-28/G/UBS/1'),
('2022-07-23/U/UBS/3', '3525015201880003', 'Ani', 'sales', '[\"ganyu5.jpg\"]', NULL, NULL, '2022-03-28/K/UBS/1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset`
--
ALTER TABLE `asset`
  ADD PRIMARY KEY (`kode_asset`);

--
-- Indexes for table `auth`
--
ALTER TABLE `auth`
  ADD PRIMARY KEY (`id_auth`);

--
-- Indexes for table `detail_asset`
--
ALTER TABLE `detail_asset`
  ADD PRIMARY KEY (`id_detail`);

--
-- Indexes for table `history_asset`
--
ALTER TABLE `history_asset`
  ADD PRIMARY KEY (`id_history`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
