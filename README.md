# Asset Management Website Application

This is a web application using php as programming languange with codeigniter 3 as framework for managing asset in UBS company

## Installation Guide 
 
### Install CodeIgniter 3

- open this link `https://www.codeigniter.com/userguide3/general/requirements.html` for more info about the installation requirements 
- open this link `https://www.codeigniter.com/download` and click download on CodeIgniter 3, and put downloaded file in htdocs

## Getting Started

### Change DB User Username and Password

- open file `./application/config/database.php` and set username, password, database name and hostname to match your DB user information

### Run The App

Open [http://localhost/Project_UBS](http://localhost/Project_UBS) with your browser to see the result or run with postman

### Access The App

#### Admin

- Open [http://localhost/Project_UBS](http://localhost/Project_UBS) on your browser and login with username and password with role admin to access the admin dashboard page (change one of user into admin in database)

- Create user and user registration on [http://localhost/Project_UBS/auth/index](http://localhost/Project_UBS) to access the user page 

#### User

- Open [http://localhost/Project_UBS](http://localhost/Project_UBS) on your browser and login with username and password based on the user registration  that has been registered by admin to access the user page

## File Location

[`./application/controllers/*`] all of the controller can be accessed on this folder

[`./application/models/*`] all of the models can be 
accessed on this folder

[`./application/views/*`] all of the views can be 
accessed on this folder

[`./application/config/*`] all of the setting for adding new library and setting configuration for database and many more can be accessed on this folder

## Learn More

To learn more about CodeIgniter 3, php, MySQL, take a look at the following resources:

- [CodeIgniter 3 Documentation](http://www.codeigniter.com/userguide3/) - learn about framework CodeIgniter 3 features and usable function
- [php Documentation](https://www.php.net/manual/en/) - a comperhensive guide to php programming languange 
- [localhost phpmyadmin Documentation](https://docs.phpmyadmin.net/en/latest/) - learn more about phpmyadmin for interacting with databases

# API DOCUMENTATION

This is a Web Application for managing asset in UBS Company using php with CodeIgniter 3 as a framework

## How To Use Postman

### Import asset_management.postman_collection.json

in Postman on collection menu click import button and then upload files asset_management.postman_collection.json in root folder

### Possible Status Code

| Status Code | What it means                                             |
| ----------- | --------------------------------------------------------- |
| 400         | `Bad Request (request body does not match form validation)`                                              |
| 404         | `Request not found (page not found or data is not available in database)`                                         |

# API Asset
it consist of eight attributes:
- kode_asset : primary key of Asset
- nama : name of asset 
- lokasi : location of asset
- kategori : it consist of five categories such as rumah_dinas, gedung, fasilitas, asrama, kendaraan
(2 examples for each API in each categories)
- status : status of asset such as available, in use and deleted
- tanggal_terima : it shows time when asset is created
- tanggal_delete : it shows time when asset is deleted
- alasan_delete : it shows the reason asset is deleted

# 1. Rumah Dinas

Rumah Dinas is one of the five category in assets data and in this category all information about official residence will be strored using this category 

## <b>1.1 Add Rumah Dinas</b>

### Description

Add new asset data with category rumah_dinas and store it in the database (only for admin).

### Method

```
POST
```

### Path URL

```
http://localhost/Project_UBS/rumah/add_rumah
```

### Example BODY

```
{
    "nama": "rumah dinas 8 * 12",
    "lokasi": "surabaya",
    "jenis": "halo",
    "kondisi": "bagus",
    "carport": "ada",
    "fasilitas": "karpet",
    "kamar_tidur": 4,
    "kamar_mandi": 2,
    "userfile": '[
        "resto21.jpg",
        "resto31.jpeg"
    ]',
    "tgl_pengadaan": "2022-03-28"
}
```

### Example Response

```
{"kode_asset":"2022-03-28\/R\/UBS\/10","nama":"rumah dinas 8 * 12","lokasi":"surabaya","kategori":"rumah_dinas","status":"available","tanggal_terima":"2022-03-28","id_detail":"D_43\/R\/UBS\/10","info_asset":"{\"jenis\":\"halo\",\"kondisi\":\"bagus\",\"carport\":\"ada\",\"fasilitas\":\"karpet\",\"kamar_mandi\":\"2\",\"kamar_tidur\":\"4\"}","foto_asset":"[\"resto21.jpg\",\"resto31.jpeg\"]"}
```

## <b>1.2 Edit Rumah Dinas</b>

### Description

Edit existing asset data with category rumah_dinas and store it in the database (only for admin).

### Method

```
POST
```

### Path URL

```
http://localhost/Project_UBS/rumah/edit_rumah
```

### Example BODY

```
{
    "nama": "rumah dinas 8 * 12",
    "lokasi": "jakarta",
    "jenis": "halo",
    "kondisi": "bagus",
    "carport": "ada",
    "fasilitas": "karpet",
    "kamar_tidur": 4,
    "kamar_mandi": 2,
    "userfile": '[
        "resto21.jpg",
        "resto31.jpeg"
    ]',
    "tgl_pengadaan": "2022-03-28"
}
```

### Example Response

```
{"kode_asset":"2022-03-28\/R\/UBS\/10","nama":"rumah dinas 8 * 12","lokasi":"jakarta","kategori":"rumah_dinas","status":"available","tanggal_terima":"2022-03-28","id_detail":"D_43\/R\/UBS\/10","info_asset":"{\"jenis\":\"halo\",\"kondisi\":\"bagus\",\"carport\":\"ada\",\"fasilitas\":\"karpet\",\"kamar_mandi\":\"2\",\"kamar_tidur\":\"4\"}","foto_asset":"[\"resto21.jpg\",\"resto31.jpeg\"]"}
```

## <b>1.3 Delete Rumah Dinas</b>

### Description

Delete existing asset data with category rumah_dinas (only for admin).

### Method

```
POST
```

### Example URL

```
http://localhost/Project_UBS/rumah/delete_rumah
```

### Example BODY

```
{
    "alasan_delete": "coba",
    "kode": "2022-03-28/R/UBS/1",
}
```

### Example Response

```
{"kode_asset":"2022-03-28\/R\/UBS\/10","nama":"rumah dinas 8 * 12","lokasi":"surabaya","kategori":"rumah_dinas","status":"deleted","tanggal_terima":"2022-03-28","id_detail":"D_18\/R\/UBS\/5","info_asset":"{\"jenis\":\"halo\",\"kondisi\":\"bagus\",\"carport\":\"ada\",\"fasilitas\":\"karpet\",\"kamar_mandi\":\"2\",\"kamar_tidur\":\"4\"}","foto_asset":"[\"resto21.jpg\",\"resto31.jpeg\"]"}
```

## <b>1.4 Fetch Rumah Admin</b>

### Description

Get draw, recordsTotal, recordsFiltered and data for datatable in rumah dinas (only for admin).

### Method

```
POST
```

### Example URL

```
http://localhost/Project_UBS/rumah/fetch_rumah_admin
```

### Example Response

```
{
    "draw": null,
    "recordsTotal": 40,
    "recordsFiltered": 10,
    "data": [
        [
            "2022-03-28/R/UBS/1",
            "rumah dinas 8 * 12",
            "jakarta",
            "2022-03-28",
            "<span style='background-color:#FFFF00;'>in use</span>",
            "<a class=\"btn btn-success btn-sm\" name=\"editButton\" data-id=\"2022-03-28/R/UBS/1\" data-bs-toggle=\"modal\" data-bs-target=\"#edit_rumah\" onclick=\"editRumah(this)\"><i class=\"bi bi-pencil-square\"></i></a>\r\n            <a class=\"btn btn-danger btn-sm \" name=\"deleteButton\" data-id=\"2022-03-28/R/UBS/1\" data-bs-toggle=\"modal\" data-bs-target=\"#delete_asset\" onclick=\"deleteClick(this)\"><i class=\"bi bi-trash\"></i></a>\r\n            <a class=\"btn btn-warning btn-sm \" name=\"addPerbaikanButton\" data-id=\"2022-03-28/R/UBS/1\" data-bs-toggle=\"modal\"  data-bs-target=\"#perbaikan_asset\" onclick=\"perbaikanAsset(this)\"><i class=\"bi bi-hammer\"></i></a>"
        ]
    ]
}
```

## <b>1.5 Fetch Rumah User</b>

### Description

Get draw, recordsTotal, recordsFiltered and data for datatable in rumah dinas with user information (only for user).

### Method

```
POST
```

### Example URL

```
http://localhost/Project_UBS/rumah/fetch_rumah_user
```

### Example Response

```
{
    "draw": null,
    "recordsTotal": 40,
    "recordsFiltered": 10,
    "data": [
        [
            "2022-03-28/R/UBS/1",
            "rumah dinas 8 * 12",
            "jakarta",
            "2022-03-28",
            null,
            "marketing",
            "<span style='background-color:#FFFF00;'>in use</span>",
            "<a type=\"button\" class=\"btn btn-primary\" name=\"detail-modal\" data-toggle=\"modal\" data-id=\"2022-03-28/R/UBS/1\" data-target=\"#detail-modal\" onclick=\"getDetail(this)\"><i class=\"bi bi-file-earmark-text\"></i></a>"
        ]
    ]
}
```

## <b>1.6 Get JSON Rumah</b>

### Description

Get json data for specific rumah dinas with its detail information.

### Method

```
POST
```

### Example URL

```
http://localhost/Project_UBS/rumah/get_rumah_json
```

### Example Body

```
{
    "kode": "2022-03-28/R/UBS/10"
}
```

### Example Response

```
{"kode_asset":"2022-03-28\/R\/UBS\/1","nama":"rumah dinas 8 *
12","lokasi":"jakarta","kategori":"rumah_dinas","status":"in
use","tanggal_terima":"2022-03-28","id_detail":"D_1\/R\/UBS\/1","info_asset":"{\"jenis\":\"hai\",\"kondisi\":\"elek\",\"carport\":\"ada\",\"fasilitas\":\"mesin
cuci\",\"kamar_mandi\":\"2\",\"kamar_tidur\":\"4\"}","foto_asset":"[\"SpringRoll.jpg\"]"}
```

# 2. Fasilitas 

Fasilitas is one of the five category in assets data and in this category all information about Facility will be strored using this category 

## <b>2.1 Add Fasilitas</b>

### Description

Add new asset data with category fasilitas and store it in the database (only for admin).

### Method

```
POST
```

### Path URL

```
http://localhost/Project_UBS/fasilitas/add_fasilitas
```

### Example BODY

```
{
    "nama": "Scanner",
    "lokasi": "surabaya",
    "jenis": "brother",
    "kondisi": "bagus",
    "userfile": '[
        "resto21.jpg",
        "resto31.jpeg"
    ]',
    "tgl_pengadaan": "2022-03-28",
    "garansi": "berlaku hingga 2023"
}
```

### Example Response

```
{"kode_asset":"2022-03-28\/F\/UBS\/10","nama":"Scanner","lokasi":"surabaya","kategori":"fasilitas","status":"available","tanggal_terima":"2022-03-28","id_detail":"D_45\/F\/UBS\/10","info_asset":"{\"jenis\":\"brother\",\"kondisi\":\"bagus\",\"garansi\":\"berlaku
hingga 2023\"}","foto_asset":"[\"download1.png\"]"}
```

## <b>2.2 Edit Fasilitas</b>

### Description

Edit existing asset data with category fasilitas and store it in the database (only for admin).

### Method

```
POST
```

### Path URL

```
http://localhost/Project_UBS/fasilitas/edit_fasilitas
```

### Example BODY

```
{
    "nama": "Scanner",
    "lokasi": "jakarta",
    "jenis": "brother",
    "kondisi": "bagus",
    "userfile": '[
        "resto21.jpg",
        "resto31.jpeg"
    ]',
    "tgl_pengadaan": "2022-03-28",
    "garansi": "berlaku hingga 2023"
}
```

### Example Response

```
{"kode_asset":"2022-03-28\/F\/UBS\/10","nama":"Scanner","lokasi":"jakarta","kategori":"fasilitas","status":"available","tanggal_terima":"2022-03-28","id_detail":"D_45\/F\/UBS\/10","info_asset":"{\"jenis\":\"brother\",\"kondisi\":\"bagus\",\"garansi\":\"berlaku
hingga 2023\"}","foto_asset":"[\"download1.png\"]"}
```

## <b>2.3 Delete Fasilitas</b>

### Description

Delete existing asset data with category rumah_dinas (only for admin).

### Method

```
POST
```

### Example URL

```
http://localhost/Project_UBS/fasilitas/delete_fasilitas
```

### Example BODY

```
{
    "alasan_delete": "coba",
    "kode": "2022-03-28/F/UBS/10",
}
```

### Example Response

```
{"kode_asset":"2022-03-28\/F\/UBS\/10","nama":"Scanner","lokasi":"jakarta","kategori":"fasilitas","status":"deleted","tanggal_terima":"2022-03-28","id_detail":"D_45\/F\/UBS\/10","info_asset":"{\"jenis\":\"brother\",\"kondisi\":\"bagus\",\"garansi\":\"berlaku
hingga 2023\"}","foto_asset":"[\"download1.png\"]"}
```

## <b>2.4 Fetch Fasilitas Admin</b>

### Description

Get draw, recordsTotal, recordsFiltered and data for datatable in fasilitas (only for admin).

### Method

```
POST
```

### Example URL

```
http://localhost/Project_UBS/fasilitas/fetch_fasilitas_admin
```

### Example Response

```
{
    "draw": null,
    "recordsTotal": 40,
    "recordsFiltered": 8,
    "data": [
        [
            "2022-03-28/F/UBS/1",
            "LCD Proyektor Coi",
            "malang",
            "2022-03-28",
            "<span style='background-color:#FF0000;'>deleted</span>",
            "<a class=\"btn btn-success btn-sm\" name=\"editButton\" data-id=\"2022-03-28/F/UBS/1\" data-bs-toggle=\"modal\" data-bs-target=\"#edit_fasilitas\" onclick=\"editFasilitas(this)\"><i class=\"bi bi-pencil-square\"></i></a>\r\n            <a class=\"btn btn-danger btn-sm \" name=\"deleteButton\" data-id=\"2022-03-28/F/UBS/1\" data-bs-toggle=\"modal\" data-bs-target=\"#delete_asset\" onclick=\"deleteClick(this)\"><i class=\"bi bi-trash\"></i></a>\r\n            <a class=\"btn btn-warning btn-sm \" name=\"addPerbaikanButton\" data-id=\"2022-03-28/F/UBS/1\" data-bs-toggle=\"modal\"  data-bs-target=\"#perbaikan_asset\" onclick=\"perbaikanAsset(this)\"><i class=\"bi bi-hammer\"></i></a>"
        ]
    ]
}
```

## <b>2.5 Fetch Fasilitas User</b>

### Description

Get draw, recordsTotal, recordsFiltered and data for datatable in fasilitas with user information (only for user).

### Method

```
POST
```

### Example URL

```
http://localhost/Project_UBS/fasilitas/fetch_fasilitas_user
```

### Example Response

```
{
    "draw": null,
    "recordsTotal": 40,
    "recordsFiltered": 10,
    "data": [
        [
            "2022-03-28/F/UBS/1",
            "LCD Proyektor Coi",
            "malang",
            "2022-03-28",
            "<span style='background-color:#FF0000;'>deleted</span>",
            null,
            null,
            "<a type=\"button\" class=\"btn btn-primary\" name=\"detail-modal\" data-toggle=\"modal\" data-id=\"2022-03-28/F/UBS/1\" data-target=\"#detail-modal\" onclick=\"getDetail(this)\"><i class=\"bi bi-file-earmark-text\"></i></a>"
        ]
    ]
}
```

## <b>2.6 Get JSON Fasilitas</b>

### Description

Get json data for specific fasilitas with its detail information.

### Method

```
POST
```

### Example URL

```
http://localhost/Project_UBS/fasilitas/get_fasilitas_json
```

### Example Body

```
{
    "kode": "2022-03-28/F/UBS/1"
}
```

### Example Response

```
{"kode_asset":"2022-03-28\/F\/UBS\/1","nama":"LCD Proyektor
Coi","lokasi":"malang","kategori":"fasilitas","status":"deleted","tanggal_terima":"2022-03-28","id_detail":"D_8\/F\/UBS\/1","info_asset":"{\"jenis\":\"LG\",\"kondisi\":\"bagus\",\"garansi\":\"berlaku
hingga 2024\"}","foto_asset":"[\"SpringRoll5.jpg\"]"}
```

# 3. Auth 

Auth is used to create, delete and login as a new user that can access user / admin page based on specified role

## <b>3.1 Add Pengguna Asset</b>

### Description

Add new user with its biodata and specific assets that is used for registration 

### Method

```
POST
```

### Path URL

```
http://localhost/Project_UBS/auth/add_pengguna_asset
```

### Example BODY

```
{
    "nik": 3525015201880006,
    "nama": "Jordan",
    "departemen": "IT",
    "userfile": '[
        "Journal_Skripsi_Jordan_Nagakusuma_c14180102_(2)8.pdf"
    ]',
    "kode_asset": "2022-03-28/F/UBS/5"
}
```

### Example Response

```
{"id_user":"2022-07-26\/U\/UBS\/3","nik":"3525015201880006","nama":"jordan","departemen":"IT","file":"[\"Journal_Skripsi_Jordan_Nagakusuma_c14180102_(2)8.pdf\"]","kode_asset":"2022-03-28\/F\/UBS\/5","nama_asset":"Scanner","lokasi":"surabaya","kategori":"fasilitas"}
```

## <b>3.2 Fetch Auth User</b>

### Description

Get draw, recordsTotal, recordsFiltered and data for datatable in user database (only for admin).

### Method

```
POST
```

### Path URL

```
http://localhost/Project_UBS/auth/fetch_asset_user
```

### Example Response

```
{
    "draw": null,
    "recordsTotal": 42,
    "recordsFiltered": 5,
    "data": [
        [
            "3525015201880002",
            "jordan",
            "marketing",
            "rumah_dinas",
            "rumah dinas 8 * 12",
            "2022-03-28/R/UBS/1",
            "jakarta",
            "[\"alice10.jpg\"]",
            "<a class=\"btn btn-danger btn-sm \" name=\"deleteButton\" data-id=\"2022-07-23/U/UBS/1\" data-bs-toggle=\"modal\" data-bs-target=\"#delete_user\" onclick=\"deleteClick(this)\"><i class=\"bi bi-trash\"></i></a>\r\n                      <a class=\"btn btn-warning btn-sm \" name=\"addRegistButton\" data-id=\"3525015201880002\" data-bs-toggle=\"modal\" data-bs-target=\"#add_regist\" onclick=\"AddRegist(this)\"><i class=\"bi bi-info\"></i></a>"
        ]
    ]
}
```

## <b>3.3 Registrasi User</b>

### Description

Add new registration information for user such as username and password that is used on login user

### Method

```
POST
```

### Path URL

```
http://localhost/Project_UBS/auth/registrasi
```

### Example BODY (password and password2 must be similar)

```
{
    "nik": 3525015201880005,
    "nama": "budi45",
    "password": "kusuma45",
    "password2": "kusuma45"
}
```

### Example Response

```
{"id_auth":"2022-07-26\/RE\/UBS\/3","username":"budi45","password":"$2y$10$wbYSKuMrAQPSrAGKeSC8O.4quFhscTFDfR4kA4p.MrVH0mv7BANkq","role":"user","id_user":"3525015201880005"}
```

## <b>3.4 Login</b>

### Description

it is used for login for admin or user (if admin login then page will be redirected to dashboard page, if user login then page will be redirected to user page)

### Method

```
POST
```

### Path URL

```
http://localhost/Project_UBS/auth/login
```

### Example BODY

```
{
    "username": "budi45",
    "password": "kusuma45",
}
```

## <b>3.5 Logout</b>

### Description

it is used for logout for admin or user

### Method

```
POST
```

### Path URL

```
http://localhost/Project_UBS/auth/logout
```

## <b>3.6 Delete Pengguna </b>

### Description

it is used to delete specific user 

### Method

```
POST
```

### Path URL

```
http://localhost/Project_UBS/auth/delete_pengguna
```

### Example Body

```
{
    "id_user": "2022-07-23/U/UBS/1",
    "alasan_delete": "coba"
}
```

### Example Response

```
{"id_user":"2022-07-23\/U\/UBS\/1","nik":"3525015201880002","nama":"jordan","departemen":"marketing","file":"[\"alice10.jpg\"]","kode_asset":"2022-03-28\/R\/UBS\/1","nama_asset":"rumah dinas 8 * 12","lokasi":"jakarta","kategori":"rumah_dinas"}
```








